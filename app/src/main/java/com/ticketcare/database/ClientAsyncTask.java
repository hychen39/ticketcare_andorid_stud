package com.ticketcare.database;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.example.o.participation.PersonalInformationView;
import com.example.o.participation.R;
import com.example.o.participation.TakingTicket;
import com.ticketcare.entities.Client;
import com.ticketcare.entities.Ticket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static junit.framework.Assert.assertNotNull;

/**
 * Created by user on 2017/11/29.
 */

public class ClientAsyncTask  extends AsyncTask<String, Integer, Integer> {
    static final String POST_RES_URL = "http://163.17.9.161:8080/TicketCareDB/webresources/entities.receiver";

    private InputStreamReader _bodyReader;
    private HttpURLConnection _conn = null;
    private String _bodyContent;
    private JSONObject jsonObject;
    private List<String> lines;

    public Fragment getOutputUI() {
        return outputUI;
    }

    public void setOutputUI(Fragment outputUI) {
        this.outputUI = outputUI;
    }

    private Fragment outputUI;


    /**
     * 取得 Response 的 Input Stream 並放到 {@link #_bodyReader}中.
     * 連線失敗時 {@code #_bodyReader} 為 null.
     */
    public void getResponseStreamReader() {
        //Create url object
        URL postResUrl = null;
        try {
            postResUrl = new URL(POST_RES_URL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        // Create connection

        try {
            _conn = (HttpURLConnection) postResUrl.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // prepare request header
        _conn.setRequestProperty("User-Agent", "Android_TicketCareDBClient");
        _conn.setRequestProperty("Accept", "application/json");

        // get response
        int responseCode = 0;
        try {
            responseCode = _conn.getResponseCode();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (responseCode == 200) {
            try {
                _bodyReader = new InputStreamReader(_conn.getInputStream(), "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else
            _bodyReader = null;
    }

    /**
     * Do the HTTP Get request. 需要在背景執行.
     *
     * @param urlStr RESTful Web Service URL
     * @return the JSON format string
     */
    public String doGetRequest(String urlStr) {
        URL targetURL = null;
        String returnValue = null;
        try {
            targetURL = new URL(urlStr);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection conn;
        try {
            conn = (HttpURLConnection) targetURL.openConnection();
            // prepare request header
            conn.setRequestProperty("User-Agent", "Android_TicketCareDBClient");
            conn.setRequestProperty("Accept", "application/json");
            conn.connect();

            int statusCode = conn.getResponseCode();
            if (statusCode == 200){
                InputStreamReader isr = new InputStreamReader(conn.getInputStream());
                try {
                    returnValue = this.getBodyContent(isr).get(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.i("123", _bodyContent);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return returnValue;
    }

    /**
     * Read the data from the input stream of the Response. Close the stream after completion.
     *
     * @param reader
     * @return
     */
    public List<String> getBodyContent(InputStreamReader reader) throws Exception {
        if (reader == null) {
            throw new Exception("The reader is null!!");
        }
        lines = new ArrayList<>();
        BufferedReader bufferReader = new BufferedReader(reader);
        String line;
        try {
            while ((line = bufferReader.readLine()) != null) {
                lines.add(line);
            }
            bufferReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

    public InputStreamReader get_bodyReader() {
        return _bodyReader;
    }

    /**
     * The backgound process to connect to web service and get the response body.
     * The response content is put in {@link #_bodyContent}
     */
    public void run() {
        this.getResponseStreamReader();
        try {
            _bodyContent = this.getBodyContent(_bodyReader).get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.i("content", _bodyContent);
    }

    /**
     * Convert {@link #_bodyContent} to JSONArray.
     * Get the body content before calling this method.
     *
     * @return
     * @see {@link #getBodyContent(InputStreamReader)} , {@link #getResponseStreamReader()}
     */
    public JSONArray getJSONObjectArray() {
        JSONArray jsonArray = null;

        try {
            jsonArray = new JSONArray(_bodyContent);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonArray;
    }

    @Override
    protected Integer doInBackground(String... strings) {
        this.run();
        return null;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        // print out the result
        JSONArray posts = getJSONObjectArray();
        Log.i("mycode", posts.toString());
        ArrayList<HashMap<String, Object>> data = new ArrayList<>();
        final ArrayList<Client> clients = new ArrayList<>();


        //放資料
        for (int i = 0; i < posts.length(); i++) {

            Client client = new Client();

            JSONObject clientJsonObject = null;
            try {
                clientJsonObject = posts.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String name;
            try {
                name = clientJsonObject.getString("name");
                client.setName(name);
            } catch (JSONException e) {
                e.printStackTrace();
                client.setName("--");
            }


            String address;
            try {
                address = clientJsonObject.getString("address");
                client.setAddress(address);

            } catch (JSONException e) {
                e.printStackTrace();
                client.setAddress("--");
            }


            String phone;
            try {
                    phone = clientJsonObject.getString("areaCode") +clientJsonObject.getString("tel");
                    client.setPhone(phone);
            } catch (JSONException e) {
                    e.printStackTrace();
                    client.setPhone("--");
            }

            String gender;
            try {
                gender = clientJsonObject.getString("gender");
                client.setSex(gender);
            } catch (JSONException e) {
                e.printStackTrace();
                client.setSex("--");
            }

            String birth;
            try {

                birth = clientJsonObject.getString("birth").substring(0,10);
                client.setBoringdate(birth);
            } catch (JSONException e) {
                e.printStackTrace();
                client.setBoringdate("--");
            }

            String note;
            try {
                note = clientJsonObject.getString("note");
                client.setNote(note);
            } catch (JSONException e) {
                e.printStackTrace();
                client.setNote("");
            }


            int id=0;
            try {

                id = clientJsonObject.getInt("id");
                client.setId(id);


            } catch (JSONException e) {
                e.printStackTrace();

            }


//            String interestsURI = "http://163.17.9.161:8080/TicketCareDB/webresources/entities.receiver/{id}/interests";
//            ReceiverFacade receiverFacade = new ReceiverFacade(new ClientAsyncTask());
//            receiverFacade.setReceiverInterestURI(interestsURI);
//            String result= receiverFacade.findInterest(client.getId());
//            assertNotNull(result);
//            JSONObject interest=null;
//
//            try {
//                interest = new JSONObject(result);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            String habit;
//            try {
//                habit = interest.getString("name");
//                client.setHabit(habit);
//            } catch (JSONException e) {
//                e.printStackTrace();
//                client.setHabit("--");
//            }


            clients.add(client);
        }

//        for(int i=0;i<clients.size();i++){
//            String habit;
//            String insterest_url = "http://163.17.9.161:8080/TicketCareDB/webresources/entities.receiver/"+clients.get(i).getId()+"/interests";
//            JSONObject jsonObject;
//            try {
//                jsonObject = new JSONObject(this.doGetRequest(insterest_url));
//                habit = jsonObject.getString("name");
//                clients.get(i).setHabit(habit);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//        }

        Log.i("mycode", "Post Size: " + posts.length());
        Log.i("mycode", "Clients Size: " + clients.size());

        // Prepare data for the SimpleAdapter
        for (
                int i = 0; i < clients.size(); i++)

        {
            HashMap<String, Object> ticketDataMap = new HashMap<String, Object>();
            ticketDataMap.put("clientName", clients.get(i).getName());
            ticketDataMap.put("clientAddress", clients.get(i).getAddress());
            ticketDataMap.put("clientPhone", clients.get(i).getPhone());
            ticketDataMap.put("clientSex", clients.get(i).getSex());
            ticketDataMap.put("clientBirth",clients.get(i).getBoringdate());
            ticketDataMap.put("clientNote",clients.get(i).getNote());
            ticketDataMap.put("clientHabit",clients.get(i).getHabit());
            ticketDataMap.put("id",clients.get(i).getId());
          ticketDataMap.put("clientHabit",clients.get(i).getHabit());
            data.add(ticketDataMap);
        }



        SimpleAdapter adapter = new SimpleAdapter(outputUI.getActivity(),
                data,
                R.layout.ticketlistlayout,
                new String[]{"clientName", "clientAddress"},
                new int[]{R.id.oneTicketsName, R.id.oneTicketsCategory});

        final ListView listView = (ListView) outputUI.getActivity().findViewById(R.id.listPersonalInformation);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new ListView.OnItemClickListener()

        {

            @Override
            public void onItemClick(final AdapterView<?> parent, View view, int position, long id) {
            final Client cliente = clients.get(position);
            Intent intent = new Intent(outputUI.getActivity(), PersonalInformationView.class);
            intent.putExtra("client",cliente);
            outputUI.startActivity(intent);
            }
        });
    }

}
