package com.ticketcare.database;

import android.os.AsyncTask;
import android.util.Log;

import com.ticketcare.database.utilities.RESTWS_Tool;
import com.ticketcare.entities.QuestionnaireEntity;
import com.ticketcare.entities.VolunteerEntity;

import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by user on 2017/12/8.
 */

public class OnequestionnaireAsyncTask extends AsyncTask<QuestionnaireEntity, Integer, String> {

    static final String QUE_RES_URL = "http://163.17.9.161:8080/TicketCareDB/webresources/entities.questionnaire";


    /**
     * Do the HTTP Get request. 需要在背景執行.
     *
     * @param urlStr RESTful Web Service URL
     * @return the JSON format string
     * @see RESTWS_Tool
     */
    public String doGetRequest(String urlStr) {
        URL targetURL = null;
        String returnValue = null;
        returnValue = RESTWS_Tool.doGetRequest(urlStr);

        return returnValue;
    }

    @Override
    protected String doInBackground(QuestionnaireEntity... questionnaireEntities) {
        String statusCode = null;
        statusCode = this.postOnequestionnaireEntity(questionnaireEntities[0]);
        return statusCode;
    }

    @Override
    protected void onPostExecute(String statusCode) {
        String msg = "VolunteerAsyncTask >> Post Volunteer with status code: " + statusCode;
        Log.i("VolunteerAsyncTask", msg);
    }

    /**
     * Post Volunteer to the URL specified by {@link #QUE_RES_URL}.
     * @param questionnaireEntity
     * @return status code
     */
    public String postOnequestionnaireEntity(QuestionnaireEntity questionnaireEntity) {

        // Prepare the JSON object
        JSONObject jsonObject = questionnaireEntity.toJsonObject();

        URL postURL = null;
        try {
            postURL = new URL(QUE_RES_URL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        String status = "", msg="";
        try {
            // open connection
            HttpURLConnection conn = (HttpURLConnection) postURL.openConnection();
            // set request method and properties
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setDoOutput(true); // 使用 connection 進行 Output, set the DoOutput flag to true.
            conn.setDoInput(true); //  使用 connection 進行 Input
            conn.connect();
            // get the output stream
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            // write the content
            os.writeBytes(jsonObject.toString());
            // flush and close output stream
            os.flush();
            os.close();
            // log response
            status = String.valueOf(conn.getResponseCode());
            msg = conn.getResponseMessage();
            Log.i("STATUS", status);
            Log.i("MSG", msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // return status code.
        return status;

    }
}
