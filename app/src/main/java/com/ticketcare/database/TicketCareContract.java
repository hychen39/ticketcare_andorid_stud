package com.ticketcare.database;

import android.provider.BaseColumns;

/** Contract class for the TicketCare DB.
 * Created by Steven Chen on 2017/4/22.
 */

public final class TicketCareContract {
    private TicketCareContract() {
    }

    public final static String DATABASE_NAME = "ticket.db";  //資料庫名稱
    public final static int DATABASE_VERSION = 11;  //資料庫版本


    /* Inner class that defines the table contents */
    public static class Clients implements BaseColumns {
        public static final String TABLE_NAME = "clients";
        public static final String COLUMN_NAME_CLIENTNAME = "name";
        public static final String COLUMN_NAME_PHONE = "phone";
        public static final String COLUMN_NAME_ADDRESS = "address";
        public static final String COLUMN_NAME_BORINGDATE = "boringdate";
        public static final String COLUMN_NAME_IDCARD = "idcard";
        public static final String COLUMN_NAME_SEXUAL = "sexual";
        public static final String COLUMN_NAME_HABIT = "habit";
        public static final String COLUMN_NAME_PROTRAIT = "prutrait";
    }

    public static class Tickets implements BaseColumns {
        public static final String TABLE_NAME = "tickets";
        public static final String COLUMN_NAME_TICKETNAME = "ticketname";
        public static final String COLUMN_NAME_TICKETCATEGORY = "ticketcategory";
        public static final String COLUMN_NAME_USINGDATE = "usingdate";
        public static final String COLUMN_NAME_DONOR = "donor";
        public static final String COLUMN_NAME_SHOWLOCATION = "showlocation";
        public static final String COLUMN_NAME_AMOUNT = "amount";
        public static final String COLUMN_NAME_SYSNOPSIS = "sysnopsis";
    }

    public static class  Dispatch implements BaseColumns{
        public static final String TABLE_NAME="dispatch";
        public static final String COLUMN_NAME_DISPATCHNAME="dispatchname";
        public static final String COLUMN_NAME_CONTENT="dispatchcontent";
        public static final String COLUMN_NAME_STATUS="dispatchstatus";
    }

    public static class ParticipationRecord implements  BaseColumns{
        public static final String TABLE_NAME="participationrecord";
        public static final String COLUMN_NAME_RECORDNAME="recordname";
        public static final String COLUMN_NAME_FREQUENCY="recordfrequency";
        public static final String COLUMN_NAME_RECEIVED="recordreveived";
    }

    public static class IsOnShelf implements BaseColumns{
        public static final String TABLE_NAME="is_on_shelf";
        public static final String COLUMN_NAME_TICKET_NAME ="name";
        public static final String COLUMN_NAME_TICKET_CATEGORY ="category";

        public static final String COLUMN_NAME_REMAIN_QTY ="remain_qty";
        public static final String COLUMN_NAME_START_DATE ="start_date";
        public static final String COLUMN_NAME_END_DATE ="end_date";
        public static final String COLUMN_NAME_REMARK ="remark";
    }

    public static class CareRecord implements  BaseColumns{
        public static final String TABLE_NAME="care_record";
        public static final String COLUMN_NAME_CLIENT_NAME = "name";
        public static final String COLUMN_NAME_HOME_ENVIRONMENT = "home_environment";
        public static final String COLUMN_NAME_DIET = "diet";
        public static final String COLUMN_NAME_HEALTH_STATUS = "health_status";
    }

    public static class Volunteer implements BaseColumns{
        public static final String TABLE_NAME = "volunteer";
        public static final String COLUMN_NAME_VOLUNTEER_NAME ="name";
        public static final String COLUMN_NAME_PHONE = "phone";
        public static final String COLUMN_NAME_PASSWORD = "password";
    }

    public static class Questionnaire implements BaseColumns {
        public static final String TABLE_NAME = "Questionnaire";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_ACTIVITY_EXPERIENCE = "activity_experience";
        public static final String COLUMN_NAME_CONTENT = "content";
        public static final String COLUMN_NAME_NEXT_CHANCE = "next_chance";
        public static final String COLUMN_NAME_PARTICIPATION = "participation";
        public static final String COLUMN_NAME_SATISFACTION = "satisfaction";
        public static final String COLUMN_NAME_TICKET_USE = "ticket_use";
        public static final String COLUMN_NAME_WHO_USE = "who_use";
        public static final String COLUMN_NAME_TICKETING_ID = "ticketing_id";
    }
}
