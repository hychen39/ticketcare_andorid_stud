package com.ticketcare.database;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.example.o.participation.OneQuestionnaire;
import com.example.o.participation.Questionnaire;
import com.example.o.participation.R;
import com.example.o.participation.TakingTicket;
import com.ticketcare.entities.Client;
import com.ticketcare.entities.QuestionnaireEntity;
import com.ticketcare.entities.TakeTicketEntity;
import com.ticketcare.entities.Ticket;
import com.ticketcare.entities.VolunteerEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;

import static com.ticketcare.database.VolunteerAsyncTask.VOL_RES_URL;

/**
 * Created by user on 2017/11/29.
 */

public class QuestionnaireAsyncTask extends AsyncTask<String, Integer, Integer> {
    static final String POST_RES_URL = "http://163.17.9.161:8080/TicketCareDB/webresources/entities.ticketingentity";
    static final String VOL_RES_URL = "http://163.17.9.161:8080/TicketCareDB/webresources/entities.questionnaire";

    private InputStreamReader _bodyReader;
    private HttpURLConnection _conn = null;
    private String _bodyContent;
    private JSONObject jsonObject;
    private List<String> lines;

    public Fragment getOutputUI() {
        return outputUI;
    }

    public void setOutputUI(Fragment outputUI) {
        this.outputUI = outputUI;
    }

    private Fragment outputUI;


    /**
     * 取得 Response 的 Input Stream 並放到 {@link #_bodyReader}中.
     * 連線失敗時 {@code #_bodyReader} 為 null.
     */
    public void getResponseStreamReader() {
        //Create url object
        URL postResUrl = null;
        try {
            postResUrl = new URL(POST_RES_URL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        // Create connection

        try {
            _conn = (HttpURLConnection) postResUrl.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // prepare request header
        _conn.setRequestProperty("User-Agent", "Android_TicketCareDBClient");
        _conn.setRequestProperty("Accept", "application/json");

        // get response
        int responseCode = 0;
        try {
            responseCode = _conn.getResponseCode();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (responseCode == 200) {
            try {
                _bodyReader = new InputStreamReader(_conn.getInputStream(), "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else
            _bodyReader = null;
    }




    /**
     * Read the data from the input stream of the Response. Close the stream after completion.
     *
     * @param reader
     * @return
     */
    public List<String> getBodyContent(InputStreamReader reader) throws Exception {
        if (reader == null) {
            throw new Exception("The reader is null!!");
        }
        lines = new ArrayList<>();
        BufferedReader bufferReader = new BufferedReader(reader);
        String line;
        try {
            while ((line = bufferReader.readLine()) != null) {
                lines.add(line);
            }
            bufferReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

    public InputStreamReader get_bodyReader() {
        return _bodyReader;
    }

    /**
     * Print the Post entity. Get the ID and the ticket address from the Post object.
     *
     * @param jsonObject Post entity object represented by the JSON format.
     * @return
     */
    public String printPostObject(JSONObject jsonObject) {
        String result = null;
        String formatStr = "%s \t %s \n";
        StringBuilder sb = new StringBuilder();
        int id = 0;
        String address = null;
        Formatter formatter = new Formatter();
        try {
            id = jsonObject.getInt("id");
        } catch (JSONException e) {
            //e.printStackTrace();
            System.out.println("No ID for this post. \n");
        }
        try {
            address = jsonObject.getJSONObject("ticketId").getString("address");
        } catch (JSONException e) {
            System.out.println("No Address for this post. \n");
        }

        formatter.format(formatStr, id, address);
        result = formatter.toString();

        return result;
    }

    /**
     * The backgound process to connect to web service and get the response body.
     * The response content is put in {@link #_bodyContent}
     */
    public void run() {
        this.getResponseStreamReader();
        try {
            _bodyContent = this.getBodyContent(_bodyReader).get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.i("content", _bodyContent);
    }

    /**
     * Convert {@link #_bodyContent} to JSONArray.
     * Get the body content before calling this method.
     *
     * @return
     * @see {@link #getBodyContent(InputStreamReader)} , {@link #getResponseStreamReader()}
     */
    public JSONArray getJSONObjectArray() {
        JSONArray jsonArray = null;

        try {
            jsonArray = new JSONArray(_bodyContent);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonArray;
    }

    /**
     * 要在背景執行的程式碼
     *
     * @param strings
     * @return
     */
    @Override
    protected Integer doInBackground(String... strings) {
        // 取得  RESTfule web service 的資料.
        this.run();
        return null;
    }

    /**
     * 背景執行完成後的回呼方法.
     *
     * @param integer
     */
    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        // print out the result
        JSONArray posts = getJSONObjectArray();
        Log.i("mycode", posts.toString());
        ArrayList<HashMap<String, Object>> data = new ArrayList<>();
        final ArrayList<Ticket> tickets = new ArrayList<>();
        final ArrayList<Client> clients = new ArrayList<>();
        final ArrayList<QuestionnaireEntity> questionnaireEntities = new ArrayList<>();

        //放資料
        for (int i = 0; i < posts.length(); i++) {

            Ticket ticket = new Ticket();
            Client client = new Client();
            QuestionnaireEntity questionnaire = new QuestionnaireEntity();
            JSONObject ticketJsonObject = null;
            JSONObject clientJsonObject = null;
            try {
                ticketJsonObject = posts.getJSONObject(i).getJSONObject("ticketId");
                clientJsonObject = posts.getJSONObject(i).getJSONObject("receiverId");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Long id;
            try {
                id = posts.getJSONObject(i).getLong("id");
                questionnaire.setId(id);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            questionnaireEntities.add(questionnaire);

            String name;
            try {
                name = ticketJsonObject.getString("name");
                ticket.setTicketName(name);
            } catch (JSONException e) {
                e.printStackTrace();
                ticket.setTicketName("--");
            }

            String category;
            try {
                category = ticketJsonObject.getJSONObject("categoryId").getString("name");
                ticket.setCategory(category);
            } catch (JSONException e) {
                e.printStackTrace();
                ticket.setCategory("--");
            }

            int qty = 0;
            try {
                qty = posts.getJSONObject(i).getInt("qty");
                ticket.setAmount(qty);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String usingdate;
            try {
                usingdate = posts.getJSONObject(i).getString("pickupdate").substring(0,10) +"  ~  " +posts.getJSONObject(i).getString("plannedpickupdate").substring(0,10);
                ticket.setUsingDate(usingdate);
            } catch (JSONException e) {
                e.printStackTrace();
                ticket.setUsingDate("--");
            }

            tickets.add(ticket);

            String clientname;
            try {
                clientname = clientJsonObject.getString("name");
                client.setName(clientname);
            } catch (JSONException e) {
                e.printStackTrace();
                client.setName("--");
            }

            String clientaddress;
            try {
                clientaddress = clientJsonObject.getString("address");
                client.setAddress(clientaddress);
            } catch (JSONException e) {
                e.printStackTrace();
                client.setAddress("--");
            }

            String clientphone;
            try {
                clientphone = clientJsonObject.getString("areaCode") + clientJsonObject.getString("tel");
                client.setPhone(clientphone);
            } catch (JSONException e) {
                e.printStackTrace();
                client.setPhone("--");
            }
            clients.add(client);
        }





        Log.i("mycode", "Post Size: " + posts.length());
        Log.i("mycode", "Tickets Size: " + tickets.size());
        Log.i("mycode", "Clients Size: " + clients.size());
        // Prepare data for the SimpleAdapter
        for ( int i = 0; i < tickets.size(); i++)

        {
            HashMap<String, Object> taketicketDataMap = new HashMap<String, Object>();
            taketicketDataMap.put("ticketingId",questionnaireEntities.get(i).getId());
            taketicketDataMap.put("ticketName", tickets.get(i).getTicketName());
            taketicketDataMap.put("category", tickets.get(i).getCategory());
            taketicketDataMap.put("amount", tickets.get(i).getAmount());
            taketicketDataMap.put("takingdate",tickets.get(i).getUsingDate());
            taketicketDataMap.put("clientName",clients.get(i).getName());
            taketicketDataMap.put("clientAddress",clients.get(i).getAddress());
            taketicketDataMap.put("clientPhone",clients.get(i).getPhone());
            data.add(taketicketDataMap);
        }

        SimpleAdapter adapter = new SimpleAdapter(outputUI.getActivity(),
                data,
                R.layout.ticketlistlayout,
                new String[]{"ticketName", "clientName"},
                new int[]{R.id.oneTicketsName, R.id.oneTicketsCategory});

         final ListView listView = (ListView) outputUI.getActivity().findViewById(R.id.questionnaire_list);
         listView.setAdapter(adapter);
         listView.setOnItemClickListener(new ListView.OnItemClickListener()
       {
//
          @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
              final Ticket currentticket = tickets.get(position);
              final Client currentclient = clients.get(position);
             final QuestionnaireEntity currentquestionnaire = questionnaireEntities.get(position);
              Intent intent = new Intent(outputUI.getActivity(), OneQuestionnaire.class);
              intent.putExtra("ticket",currentticket);
              intent.putExtra("client",currentclient);
             intent.putExtra("questionnaire",currentquestionnaire);
              outputUI.startActivity(intent);
                              }

            });
        }





    public String postQuestionnaireEntity(QuestionnaireEntity questionnaireEntity) {

        // Prepare the JSON object
        JSONObject jsonObject = questionnaireEntity.toJsonObject();

        URL postURL = null;
        try {
            postURL = new URL(VOL_RES_URL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        String status = "", msg="";
        try {
            // open connection
            HttpURLConnection conn = (HttpURLConnection) postURL.openConnection();
            // set request method and properties
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setDoOutput(true); // 使用 connection 進行 Output, set the DoOutput flag to true.
            conn.setDoInput(true); //  使用 connection 進行 Input
            conn.connect();
            // get the output stream
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            // write the content
            os.writeBytes(jsonObject.toString());
            // flush and close output stream
            os.flush();
            os.close();
            // log response
            status = String.valueOf(conn.getResponseCode());
            msg = conn.getResponseMessage();
            Log.i("STATUS", status);
            Log.i("MSG", msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // return status code.
        return status;

    }


}
