package com.ticketcare.database;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.example.o.participation.R;
import com.example.o.participation.TicketsView;
import com.ticketcare.entities.Ticket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;

public class TicketCareDBClient extends AsyncTask<String, Integer, Integer> {
    static final String POST_RES_URL = "http://163.17.9.161:8080/TicketCareDB/webresources/entities.post";
    private InputStreamReader _bodyReader;
    private HttpURLConnection _conn = null;
    private String _bodyContent;
    private JSONObject jsonObject;
    private List<String> lines;

    public Fragment getOutputUI() {
        return outputUI;
    }

    public void setOutputUI(Fragment outputUI) {
        this.outputUI = outputUI;
    }

    private Fragment outputUI;


    /**
     * 取得 Response 的 Input Stream 並放到 {@link #_bodyReader}中.
     * 連線失敗時 {@code #_bodyReader} 為 null.
     */
    public void getResponseStreamReader() {
        //Create url object
        URL postResUrl = null;
        try {
            postResUrl = new URL(POST_RES_URL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        // Create connection

        try {
            _conn = (HttpURLConnection) postResUrl.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // prepare request header
        _conn.setRequestProperty("User-Agent", "Android_TicketCareDBClient");
        _conn.setRequestProperty("Accept", "application/json");

        // get response
        int responseCode = 0;
        try {
            responseCode = _conn.getResponseCode();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (responseCode == 200) {
            try {
                _bodyReader = new InputStreamReader(_conn.getInputStream(), "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else
            _bodyReader = null;
    }




    /**
     * Read the data from the input stream of the Response. Close the stream after completion.
     *
     * @param reader
     * @return
     */
    public List<String> getBodyContent(InputStreamReader reader) throws Exception {
        if (reader == null) {
            throw new Exception("The reader is null!!");
        }
        lines = new ArrayList<>();
        BufferedReader bufferReader = new BufferedReader(reader);
        String line;
        try {
            while ((line = bufferReader.readLine()) != null) {
                lines.add(line);
            }
            bufferReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

    public InputStreamReader get_bodyReader() {
        return _bodyReader;
    }

    /**
     * Print the Post entity. Get the ID and the ticket address from the Post object.
     *
     * @param jsonObject Post entity object represented by the JSON format.
     * @return
     */
    public String printPostObject(JSONObject jsonObject) {
        String result = null;
        String formatStr = "%s \t %s \n";
        StringBuilder sb = new StringBuilder();
        int id = 0;
        String address = null;
        Formatter formatter = new Formatter();
        try {
            id = jsonObject.getInt("id");
        } catch (JSONException e) {
            //e.printStackTrace();
            System.out.println("No ID for this post. \n");
        }
        try {
            address = jsonObject.getJSONObject("ticketId").getString("address");
        } catch (JSONException e) {
            System.out.println("No Address for this post. \n");
        }

        formatter.format(formatStr, id, address);
        result = formatter.toString();

        return result;
    }

    /**
     * The backgound process to connect to web service and get the response body.
     * The response content is put in {@link #_bodyContent}
     */
    public void run() {
        this.getResponseStreamReader();
        try {
            _bodyContent = this.getBodyContent(_bodyReader).get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.i("content", _bodyContent);
    }

    /**
     * Convert {@link #_bodyContent} to JSONArray.
     * Get the body content before calling this method.
     *
     * @return
     * @see {@link #getBodyContent(InputStreamReader)} , {@link #getResponseStreamReader()}
     */
    public JSONArray getJSONObjectArray() {
        JSONArray jsonArray = null;

        try {
            jsonArray = new JSONArray(_bodyContent);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonArray;
    }

    /**
     * 要在背景執行的程式碼
     *
     * @param strings
     * @return
     */
    @Override
    protected Integer doInBackground(String... strings) {
        // 取得  RESTfule web service 的資料.
        this.run();
        return null;
    }

    /**
     * 背景執行完成後的回呼方法.
     *
     * @param integer
     */
    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        // print out the result
        JSONArray posts = getJSONObjectArray();
        Log.i("mycode", posts.toString());
        ArrayList<HashMap<String, Object>> data = new ArrayList<>();
        final ArrayList<Ticket> tickets = new ArrayList<>();

        //放資料
        for (int i = 0; i < posts.length(); i++) {

            Ticket ticket = new Ticket();

            JSONObject ticketJsonObject = null;
            try {
                ticketJsonObject = posts.getJSONObject(i).getJSONObject("ticketId");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String name;
            try {
                name = ticketJsonObject.getString("name");
                ticket.setTicketName(name);
            } catch (JSONException e) {
                e.printStackTrace();
                ticket.setTicketName("--");
            }

            String category;
            try {
                category = ticketJsonObject.getJSONObject("categoryId").getString("name");
                ticket.setCategory(category);
            } catch (JSONException e) {
                e.printStackTrace();
                ticket.setCategory("--");
            }

            String address;
            try {
                address = ticketJsonObject.getString("address");
                ticket.setShowLocation(address);
            } catch (JSONException e) {
                e.printStackTrace();
                ticket.setShowLocation("--");
            }

            int qty = 0;
            try {
                qty = ticketJsonObject.getInt("qty");
                ticket.setAmount(qty);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String content;
            try {
                content = ticketJsonObject.getString("content");
                ticket.setSynopsis(content);
            } catch (JSONException e) {
                e.printStackTrace();
                ticket.setSynopsis("--");
            }

            String donor;
            try {
                donor = ticketJsonObject.getJSONObject("donorId").getString("donorname");
                ticket.setDonor(donor);
            } catch (JSONException e) {
                e.printStackTrace();
                ticket.setDonor("--");
            }

            String usingdate;
            try {
                usingdate = ticketJsonObject.getString("createDate").substring(0,10) +"  ~  " +ticketJsonObject.getString("enddate").substring(0,10);
                ticket.setUsingDate(usingdate);
            } catch (JSONException e) {
                e.printStackTrace();
                ticket.setUsingDate("--");
            }

            tickets.add(ticket);
        }



        Log.i("mycode", "Post Size: " + posts.length());
        Log.i("mycode", "Tickets Size: " + tickets.size());
        // Prepare data for the SimpleAdapter
        for (
                int i = 0; i < tickets.size(); i++)

        {
            HashMap<String, Object> ticketDataMap = new HashMap<String, Object>();
            ticketDataMap.put("ticketName", tickets.get(i).getTicketName());
            ticketDataMap.put("category", tickets.get(i).getCategory());
            ticketDataMap.put("showLocation", tickets.get(i).getShowLocation());
            ticketDataMap.put("amount", tickets.get(i).getAmount());
            ticketDataMap.put("usingdate",tickets.get(i).getUsingDate());
            ticketDataMap.put("synopsis",tickets.get(i).getSynopsis());
            ticketDataMap.put("donor",tickets.get(i).getDonor());
            data.add(ticketDataMap);
        }

        SimpleAdapter adapter = new SimpleAdapter(outputUI.getActivity(),
                data,
                R.layout.ticketlistlayout,
                new String[]{"ticketName", "category"},
                new int[]{R.id.oneTicketsName, R.id.oneTicketsCategory});

       final ListView listView = (ListView) outputUI.getActivity().findViewById(R.id.listTickets);
     listView.setAdapter(adapter);
      listView.setOnItemClickListener(new ListView.OnItemClickListener()
     {

         @Override
         public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
             final Ticket currentticket = tickets.get(position);
             Intent intent = new Intent(outputUI.getActivity(), TicketsView.class);
             intent.putExtra("ticket",currentticket);
             outputUI.startActivity(intent);

         }
     });

        // convert jsonarray to string array for showing in the list view.
//        List<String> contents = new ArrayList<>();
//        for (int i=0; i < posts.length(); i++){
//            try {
//                contents.add(posts.getString(i));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//        // set the content to view adaptor
//
//        ArrayAdapter<String> itemList = new ArrayAdapter<String>(outputUI.getContext(),
//                android.R.layout.simple_list_item_1,
//                contents.toArray(new String[0]));
//        // Set the adaptor to the list view
//        ListView listView = (ListView) outputUI.getActivity().findViewById(R.id.take_ticket_list);
//        listView.setAdapter(itemList);
//
//        Log.i("TicketCareDBClient", "Update the list view");
//
//        listView.setOnItemClickListener(listener);
//    }
//
//    ListView.OnItemClickListener listener = new ListView.OnItemClickListener(){
//
//        @Override
//        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            Intent intent = new Intent(outputUI.getActivity(),Home.class);
//            outputUI.startActivity(intent);
//        }

    }


}