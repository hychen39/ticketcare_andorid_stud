package com.ticketcare.database.utilities;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/** Get the receiver's interests in the background by giving the receiver id.
 * Get the return result by calling {@link #getInterestJSONfmt()} after the AsynTask status is FINISHED.
 *
 * Created by steven on 12/6/2017.
 */

public class ClientInterestAsyncTask extends AsyncTask<Integer, Integer, String> {

    private String receiverInterestURI = "http://163.17.9.161:8080/TicketCareDB/webresources/entities.receiver/{id}/interests";
    private String interestJSONfmt = null;

    public String getReceiverInterestURI() {
        return receiverInterestURI;
    }

    public void setReceiverInterestURI(String receiverInterestURI) {
        this.receiverInterestURI = receiverInterestURI;
    }

    public String getInterestJSONfmt() {
        return interestJSONfmt;
    }

    @Override
    protected String doInBackground(Integer... receiverIDs) {
        int id = receiverIDs[0];
        String finalURI = this.receiverInterestURI.replace("{id}", String.valueOf(id));
        String result = RESTWS_Tool.doGetRequest(finalURI);
        JSONArray jsonArray = null;
        JSONObject jsonObject = null;
        String interestname = null;
        try {

            jsonObject =  new JSONObject(result);
            interestname  = jsonObject.getString("name");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        interestJSONfmt = result;
        return;

    }

}
