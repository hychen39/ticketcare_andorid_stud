package com.ticketcare.database.utilities;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/** RESTful Web Service 的工具程式
 * Created by steven on 12/6/2017.
 */

public class RESTWS_Tool {
    /**
     * Do the HTTP Get request. 需要在背景執行.
     *
     * @param urlStr RESTful Web Service URL
     * @return the JSON format string
     */
    public static String doGetRequest(String urlStr) {
        URL targetURL = null;
        String returnValue = null;
        JSONArray jsonArray = null;
        JSONObject jsonObject = null;
        String interestname = null;
        try {
            targetURL = new URL(urlStr);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection conn;
        try {
            conn = (HttpURLConnection) targetURL.openConnection();
            // prepare request header
            conn.setRequestProperty("User-Agent", "Android_TicketCareDBClient");
            conn.setRequestProperty("Accept", "application/json");
            conn.connect();

            int statusCode = conn.getResponseCode();
            if (statusCode == 200){
                InputStreamReader isr = new InputStreamReader(conn.getInputStream());
                try {
                    returnValue = getBodyContent(isr).get(0);
                    jsonArray = new JSONArray(returnValue);
                    jsonObject = jsonArray.getJSONObject(0);
                    interestname = jsonObject.getString("name");


                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.i("doGetRequest >>", returnValue);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return interestname;
    }

    /**
     * Read the data from the input stream of the Response. Close the stream after completion.
     *
     * @param reader
     * @return
     */
    private static List<String> getBodyContent(InputStreamReader reader) throws Exception {
        if (reader == null) {
            throw new Exception("The reader is null!!");
        }
        List<String> lines = new ArrayList<>();
        BufferedReader bufferReader = new BufferedReader(reader);
        String line;
        try {
            while ((line = bufferReader.readLine()) != null) {
                lines.add(line);
            }
            bufferReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }


}
