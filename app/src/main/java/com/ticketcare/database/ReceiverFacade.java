package com.ticketcare.database;

import android.util.Log;

import com.ticketcare.database.utilities.ClientInterestAsyncTask;

import java.util.concurrent.ExecutionException;

/**
 * Created by user on 2017/12/5.
 */

public class ReceiverFacade {
    private ClientAsyncTask wsClient;

        /**
     * Find the receiver's interest.
     * @param receiverID
     * @return String of the JSON format.
         * @see ClientInterestAsyncTask
     */
    public String findInterest(int receiverID){
        // get the interest using the async thread.
        ClientInterestAsyncTask clientInterestAsyncTask = new ClientInterestAsyncTask();
        clientInterestAsyncTask.execute(receiverID);
        Log.d("findInterest >>", "Get interests for the receiver: " + receiverID);
//        while (clientInterestAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
//            Log.d("findInterest >>", "Wait till the end of the background process...");
//        }
        // get result
        String result = null;
        try {
            result = clientInterestAsyncTask.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return result;
    }

}
