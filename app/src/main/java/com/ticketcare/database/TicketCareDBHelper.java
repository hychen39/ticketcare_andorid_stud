package com.ticketcare.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Helper class for the TicketCase database
 */
public class TicketCareDBHelper extends SQLiteOpenHelper{

    /** @deprecated      */
    private final static String DATABASE_NAME = "demo.db";  //資料庫名稱
    /** @deprecated  */
    private final static int DATABASE_VERSION = 1;  //資料庫版本

    private final static String SQL_CREATE_CLIENT_TABLE = "Create Table " + TicketCareContract.Clients.TABLE_NAME +
            " (" + TicketCareContract.Clients._ID + " INTEGER PRIMARY KEY, " +
            TicketCareContract.Clients.COLUMN_NAME_CLIENTNAME + " Text, " +
            TicketCareContract.Clients.COLUMN_NAME_PHONE + " Text, " +
            TicketCareContract.Clients.COLUMN_NAME_ADDRESS + " Text, " +
            TicketCareContract.Clients.COLUMN_NAME_BORINGDATE + " Text, " +
            TicketCareContract.Clients.COLUMN_NAME_IDCARD + " Text, " +
            TicketCareContract.Clients.COLUMN_NAME_SEXUAL + " Text, " +
            TicketCareContract.Clients.COLUMN_NAME_HABIT + " Text, " +
            TicketCareContract.Clients.COLUMN_NAME_PROTRAIT + " BLOB ) ";

    private final static String SQL_CREATE_TICKET_TABLE = "Create Table " + TicketCareContract.Tickets.TABLE_NAME +
            " (" + TicketCareContract.Tickets._ID + " INTEGER PRIMARY KEY, " +
            TicketCareContract.Tickets.COLUMN_NAME_TICKETNAME + " Text, " +
            TicketCareContract.Tickets.COLUMN_NAME_TICKETCATEGORY+ " Text, " +
            TicketCareContract.Tickets.COLUMN_NAME_USINGDATE + " Text, " +
            TicketCareContract.Tickets.COLUMN_NAME_DONOR + " Text, " +
            TicketCareContract.Tickets.COLUMN_NAME_SHOWLOCATION + " Text, " +
            TicketCareContract.Tickets.COLUMN_NAME_AMOUNT + " INTEGER, " +
            TicketCareContract.Tickets.COLUMN_NAME_SYSNOPSIS+ " Text) ";

    private final static String SQL_CREATE_DISPATCH_TABLE="Create Table "+TicketCareContract.Dispatch.TABLE_NAME+
            "(" + TicketCareContract.Dispatch._ID +"  INTEGER PRIMARY KEY, "+
            TicketCareContract.Dispatch.COLUMN_NAME_DISPATCHNAME + " Text, " +
            TicketCareContract.Dispatch.COLUMN_NAME_CONTENT+ " Text, " +
            TicketCareContract.Dispatch.COLUMN_NAME_STATUS + " Boolean) ";

    private final static String SQL_CREATE_PARTICIPATIONRECORD_TABLE="Create Table  "+TicketCareContract.ParticipationRecord.TABLE_NAME+
            "(" + TicketCareContract.ParticipationRecord._ID +"  INTEGER PRIMARY KEY,  "+
            TicketCareContract.ParticipationRecord.COLUMN_NAME_RECORDNAME + " Text, " +
            TicketCareContract.ParticipationRecord.COLUMN_NAME_FREQUENCY+ "  INTEGER, " +
            TicketCareContract.ParticipationRecord.COLUMN_NAME_RECEIVED + " INTEGER) ";

    private final static String SQL_CREATE_ISONSHELF_TABLE="Create Table  "+ TicketCareContract.IsOnShelf.TABLE_NAME+
            "("  + TicketCareContract.IsOnShelf._ID +" INTEGER PRIMARY KEY,  "+
            TicketCareContract.IsOnShelf.COLUMN_NAME_TICKET_NAME +  " Text,   "+
            TicketCareContract.IsOnShelf.COLUMN_NAME_TICKET_CATEGORY +  " Text,  "+
            TicketCareContract.IsOnShelf.COLUMN_NAME_REMAIN_QTY + " INTEGER,  " +
            TicketCareContract.IsOnShelf.COLUMN_NAME_START_DATE + " Text,  "  +
            TicketCareContract.IsOnShelf.COLUMN_NAME_END_DATE +  " Text,  "  +
            TicketCareContract.IsOnShelf.COLUMN_NAME_REMARK +  " Text)" ;

    private final static String SQL_CREATE_CARERECORD_TABLE="Create Table  "+ TicketCareContract.CareRecord.TABLE_NAME+
            "("  + TicketCareContract.CareRecord._ID +" INTEGER PRIMARY KEY,  "+
            TicketCareContract.CareRecord.COLUMN_NAME_CLIENT_NAME +  " Text,   "+
            TicketCareContract.CareRecord.COLUMN_NAME_HOME_ENVIRONMENT +  " Text,   "+
            TicketCareContract.CareRecord.COLUMN_NAME_HEALTH_STATUS +  " Text,   " +
            TicketCareContract.CareRecord.COLUMN_NAME_DIET +  " Text)  ";

    private final static String SQL_CREATE_VOLUNTEER_TABLE="Create Table  "+ TicketCareContract.Volunteer.TABLE_NAME+
            "("  + TicketCareContract.Volunteer._ID +" INTEGER PRIMARY KEY,  "+
            TicketCareContract.Volunteer.COLUMN_NAME_VOLUNTEER_NAME +  " Text,   "+
            TicketCareContract.Volunteer.COLUMN_NAME_PHONE + " Text,  "+
            TicketCareContract.Volunteer.COLUMN_NAME_PASSWORD + " Text)  ";

    private final static String SQL_CREATE_QUESTIONNAIRE_TABLE="Create Table "+TicketCareContract.Questionnaire.TABLE_NAME+
            "(" + TicketCareContract.Questionnaire._ID +"  INTEGER PRIMARY KEY, "+
            TicketCareContract.Questionnaire.COLUMN_NAME_TICKET_USE + " Text, " +
            TicketCareContract.Questionnaire.COLUMN_NAME_ACTIVITY_EXPERIENCE+ " Text, " +
            TicketCareContract.Questionnaire.COLUMN_NAME_WHO_USE+ " Text, " +
            TicketCareContract.Questionnaire.COLUMN_NAME_PARTICIPATION+ " Text, " +
            TicketCareContract.Questionnaire.COLUMN_NAME_SATISFACTION+ " Text, " +
            TicketCareContract.Questionnaire.COLUMN_NAME_NEXT_CHANCE+ " Text, " +
            TicketCareContract.Questionnaire.COLUMN_NAME_CONTENT+ " Text, " +
            TicketCareContract.Questionnaire.COLUMN_NAME_TICKETING_ID+ " Text) " ;

    private static final String SQL_DELETE_CLIENT_TABLE =
            "DROP TABLE IF EXISTS  " + TicketCareContract.Clients.TABLE_NAME;

    private static final String SQL_DELETE_TICKET_TABLE =
            "DROP TABLE IF EXISTS  " + TicketCareContract.Tickets.TABLE_NAME;

    private static final String SQL_DELETE_DISPATCH_TABLE=
            "DROP TABLE IF EXISTS  "+TicketCareContract.Dispatch.TABLE_NAME;

    private static final String SQL_DELETE_PARTICIPATIONRECORD_TABLE=
            "DROP TABLE IF EXISTS  " + TicketCareContract.ParticipationRecord.TABLE_NAME;

    private static final String SQL_DELETE_ISONSHELF_TABLE=
            "DROP TABLE IF  EXISTS  "  +  TicketCareContract.IsOnShelf.TABLE_NAME;

    private static final String SQL_DELETE_CARERECORD_TABLE=
            "DROP TABLE IF  EXISTS  "  +  TicketCareContract.CareRecord.TABLE_NAME;

    private static final String SQL_DELETE_VOLUNTEER_TABLE=
            "DROP TABLE IF  EXISTS  "  +  TicketCareContract.Volunteer.TABLE_NAME;

    private static final String SQL_DELETE_QUESTIONNAIRE_TABLE =
            "DROP TABLE IF EXISTS  " + TicketCareContract.Questionnaire.TABLE_NAME;

    public TicketCareDBHelper(Context context) {
        super(context,TicketCareContract.DATABASE_NAME, null, TicketCareContract.DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        createDBSQL(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        deleteDBSQL(db);
        createDBSQL(db);


    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    private void createDBSQL(SQLiteDatabase db){
        db.execSQL(SQL_CREATE_CLIENT_TABLE);
        db.execSQL(SQL_CREATE_TICKET_TABLE);
        db.execSQL(SQL_CREATE_DISPATCH_TABLE);
        db.execSQL(SQL_CREATE_PARTICIPATIONRECORD_TABLE);
        db.execSQL(SQL_CREATE_ISONSHELF_TABLE);
        db.execSQL(SQL_CREATE_CARERECORD_TABLE);
        db.execSQL(SQL_CREATE_VOLUNTEER_TABLE);
        Log.i(TicketCareDBHelper.class.getName(), "Execute SQL Statement: " + SQL_CREATE_CLIENT_TABLE);
        Log.i(TicketCareDBHelper.class.getName(), "Execute SQL Statement: " + SQL_CREATE_TICKET_TABLE);
        Log.i(TicketCareDBHelper.class.getName(),"ExecuteSQL Statement:  "+ SQL_CREATE_DISPATCH_TABLE);
        Log.i(TicketCareDBHelper.class.getName(),"ExecuteSQL Statement:  "+ SQL_CREATE_PARTICIPATIONRECORD_TABLE);
        Log.i(TicketCareDBHelper.class.getName(), "ExecuteSQL Statement:  "  +  SQL_CREATE_ISONSHELF_TABLE);
        Log.i(TicketCareDBHelper.class.getName(), "ExecuteSQL Statement:  "  +  SQL_CREATE_CARERECORD_TABLE);
        Log.i(TicketCareDBHelper.class.getName(), "ExecuteSQL Statement:  "  +  SQL_CREATE_VOLUNTEER_TABLE);
    }

    private void deleteDBSQL(SQLiteDatabase db){
        db.execSQL(SQL_DELETE_CLIENT_TABLE);
        db.execSQL(SQL_DELETE_TICKET_TABLE);
        db.execSQL(SQL_DELETE_DISPATCH_TABLE);
        db.execSQL(SQL_DELETE_PARTICIPATIONRECORD_TABLE);
        db.execSQL(SQL_DELETE_ISONSHELF_TABLE);
        db.execSQL(SQL_DELETE_CARERECORD_TABLE);
        db.execSQL(SQL_DELETE_VOLUNTEER_TABLE);
    }
}