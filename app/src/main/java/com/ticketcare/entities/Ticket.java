package com.ticketcare.entities;

import java.io.Serializable;

/**
 * Created by O on 2017/3/14.
 */

public class Ticket implements Serializable {
    private String category;
    private String ticketName;
    private String usingDate;
    private String donor;
    private String matchPerson;
    private String showLocation;
    private String synopsis;
    private int amount;
    private int picture;




    public String getCategory(){return category;}

    public void setCategory(String category){this.category = category;}

    public String getTicketName()  {
        return ticketName;
    }

    public void setTicketName(String ticketName) {
        this.ticketName = ticketName;
    }

    public String getUsingDate() {
        return usingDate;
    }

    public void setUsingDate(String usingDate) {
        this.usingDate = usingDate;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getPicture() {
        return picture;
    }

    public void setPicture(int picture) {
        this.picture = picture;
    }

    public String getDonor() {
        return donor;
    }

    public void setDonor(String donor) {
        this.donor = donor;
    }

    public String getShowLocation(){return showLocation;}

    public void setShowLocation(String showLocation){this.showLocation = showLocation;}

    public String getSynopsis(){return synopsis;}

    public void setSynopsis(String synopsis){this.synopsis = synopsis;}

    public String getMatchPerson()  {
        return matchPerson;
    }

    public void setMatchPerson(String matchPerson) {
        this.matchPerson = matchPerson;
    }
}
