package com.ticketcare.entities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by user on 2017/12/3.
 */

public class VolunteerEntity implements Serializable {
    private Long id;
    private String name;
    private String password;
    private String phone;


    public void setId(Long id){this.id = id;}
    public Long getId(){return id;}

    public void setName(String name){this.name = name;}
    public String getName(){return name;}

    public void setPhone(String phone){this.phone = phone;}
    public String getPhone(){return phone;}

    public void setPassword(String password){this.password = password;}
    public String getPassword(){return password;}

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof VolunteerEntity)) {
            // inconsistent type
            return false;
        }

        VolunteerEntity other = (VolunteerEntity) obj;
        if ((this.id == null && other.id != null) || this.id != null && !this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

    /**
     * Conver the object to JSON object
     *
     * @return JSON object
     */
    public JSONObject toJsonObject() {
        // get bean property info
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", this.id);
            jsonObject.put("name", this.name);
            jsonObject.put("password",this.password);
            jsonObject.put("phone",this.phone);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

}
