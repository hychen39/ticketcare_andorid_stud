package com.ticketcare.entities;

import android.widget.EditText;
import android.widget.RadioGroup;

import com.example.o.participation.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by user on 2017/12/4.
 */

public class QuestionnaireEntity implements Serializable {

    private Long ticketingId;
    private String ifusing_ornot;
    private String if_hadbeen;
    private String if_gowith;
    private String if_upgrade;
    private String if_satisfaction;
    private String if_continue;
    private String editNote;
            private Long id;

    public void setId(Long id){this.id = id;}
    public Long getId(){return id;}

    public void setTicketingId(Long ticketingid){this.ticketingId = id;}
    public Long getTicketingId(){return ticketingId;}

    public void setIfusing_ornot(String ifusing_ornot){this.ifusing_ornot = ifusing_ornot;}
    public String getIfusing_ornot(){return ifusing_ornot;}

    public void setIf_hadbeen(String if_hadbeen){this.if_hadbeen = if_hadbeen;}
    public String getIf_hadbeen(){return if_hadbeen;}

    public void setIf_gowith(String if_gowith){this.if_gowith = if_gowith;}
    public String getIf_gowith(){return if_gowith;}

    public void setIf_upgrade(String if_upgrade){this.if_upgrade = if_upgrade;}
    public String getIf_upgrade(){return if_upgrade;}

    public void setIf_satisfaction(String if_satisfaction){this.if_satisfaction = if_satisfaction;}
    public String getIf_satisfaction(){return if_satisfaction;}

    public void setIf_continue(String if_continue){this.if_continue = if_continue;}
    public String getIf_continue(){return if_continue;}

    public void setEditNote(String editNote){this.editNote = editNote;}
    public String getEditNote(){return editNote;}

    public int hashCode() {
        int hash = 0;
         hash += (ticketingId != null ? ticketingId.hashCode() : 0);
        return hash;
}

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof QuestionnaireEntity)) {
            // inconsistent type
            return false;
        }

        QuestionnaireEntity other = (QuestionnaireEntity) obj;
        if ((this.ticketingId == null && other.ticketingId != null) || this.ticketingId != null && !this.ticketingId.equals(other.ticketingId)) {
            return false;
        }
        return true;
    }

    /**
     * Conver the object to JSON object
     *
     * @return JSON object
     */
    public JSONObject toJsonObject() {
        // get bean property info
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id",this.id);
            jsonObject.put("ticketing_Id", this.ticketingId);
            jsonObject.put("ticket_use", this.ifusing_ornot);
            jsonObject.put("activity_experience",this.if_hadbeen);
            jsonObject.put("who_use",this.if_gowith);
            jsonObject.put("participation",this.if_upgrade);
            jsonObject.put("satisfaction",this.if_satisfaction);
            jsonObject.put("next_chance",this.if_continue);
            jsonObject.put("content",this.editNote);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

}
