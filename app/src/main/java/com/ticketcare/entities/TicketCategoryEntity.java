package com.ticketcare.entities;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 票卷類別
 * Created by steven on 11/30/2017.
 */

public class TicketCategoryEntity {
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TicketCategoryEntity)) {
            // inconsistent type
            return false;
        }

        TicketCategoryEntity other = (TicketCategoryEntity) obj;
        if ((this.id == null && other.id != null) || this.id != null && !this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

    /**
     * Conver the object to JSON object
     *
     * @return JSON object
     */
    public JSONObject toJsonObject() {
        // get bean property info
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", this.id);
            jsonObject.put("name", this.name);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }
}
