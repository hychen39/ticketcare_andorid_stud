package com.ticketcare.entities;

import com.example.o.participation.TakeTicket;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by user on 2017/12/4.
 */

public class TakeTicketEntity {

    private Long id;
    private Date date;
    private String ticketingId;
    private String volunteerId;


    public void setId(Long id){this.id = id;}
    public Long getId(){return id;}

    public void setDate(Date date){this.date = date;}
    public Date getDate(){return date;}

    public void setTicketingId(String ticketingId){this.ticketingId = ticketingId;}
    public String getTicketingId(){return ticketingId;}

    public void setVolunteerId(String volunteerId){this.volunteerId = volunteerId;}
    public String getVolunteerId(){return volunteerId;}

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof VolunteerEntity)) {
            // inconsistent type
            return false;
        }

        TakeTicketEntity other = (TakeTicketEntity) obj;
        if ((this.id == null && other.id != null) || this.id != null && !this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

    /**
     * Conver the object to JSON object
     *
     * @return JSON object
     */
    public JSONObject toJsonObject() {
        // get bean property info
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", this.id);
            jsonObject.put("date", this.date);
            jsonObject.put("ticketingId",this.ticketingId);
            jsonObject.put("volunteerId",this.volunteerId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }
}
