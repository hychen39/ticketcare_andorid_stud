package com.ticketcare.entities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Formatter;

/** Post Entity 票卷刊登類別
 * Created by steven on 11/30/2017.
 */

public class PostEntity {

    /**
     * Generate a PostEntity object by parsing the given JSON Object.
     * @param jsonObject PostEntity expressed in the JSON format
     * @return PostEntity Object
     *  ToDo: TBD
     */
    public static PostEntity parse(JSONObject jsonObject){
        return null;
    }

    /**
     * Print the Post entity. Get the ID and the ticket address from the Post object.
     *
     * @param jsonObject Post entity object represented by the JSON format.
     * @return
     */
    public static String printPostObject(JSONObject jsonObject) {
        String result = null;
        String formatStr = "%s \t %s \n";
        StringBuilder sb = new StringBuilder();
        int id = 0;
        String address = null;
        Formatter formatter = new Formatter();
        try {
            id = jsonObject.getInt("id");
        } catch (JSONException e) {
            //e.printStackTrace();
            System.out.print("No ID for this post. \n");
        }

        try {
            address = jsonObject.getJSONObject("ticketId").getString("address");
        } catch (JSONException e) {
            System.out.print("No Address for this post. \n");
        }

        formatter.format(formatStr, id, address);
        result = formatter.toString();

        return result;
    }
}
