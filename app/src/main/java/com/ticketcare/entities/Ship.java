package com.ticketcare.entities;

import java.io.Serializable;

/**
 * Created by user on 2017/8/4.
 */

public class Ship implements Serializable {
    private String name;
    private String context;
    private String status;
    public String getName()  {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status =status;
    }
}
