package com.ticketcare.entities;

import java.io.Serializable;

/**
 * Created by Steven Chen on 2017/2/24.
 *  Client 案主， 受助人。
 */

public class Client implements Serializable {
    private String name;
    private String phone;
    private String address;
    private String sex;
    private String habit;
    private String boringdate;
    private String idcard;
    private String note;
    private int id;
    private byte[] picture;

    public int getId()  {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getName()  {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getHabit() {
        return habit;
    }

    public void setHabit(String habit) {
        this.habit = habit;
    }

    public String getBoringdate() {
        return boringdate;
    }

    public void setBoringdate(String boringdate) {this.boringdate = boringdate;}

    public String getIdcard() {return idcard;}

    public void setIdcard(String idcard) {this.idcard =idcard;}

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public void setNote(String note){this.note = note;}

    public String getNote(){return note;}
}
