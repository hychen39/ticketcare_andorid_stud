package com.example.o.participation;

import android.content.ContentValues;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ticketcare.database.TicketCareContract;
import com.ticketcare.database.TicketCareDBHelper;

/**
 * 新增已上架票卷資料的 Activity
 */
public class IsOnShelfInsert extends AppCompatActivity {
    private TicketCareDBHelper ticketCareDBHelper;
    Button insertbtn;
    EditText editpostname,editpostcategory, editpostremain, editstartdate, editenddate, editremark;
    TextView dbmsg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isonshelf_insert);
        ticketCareDBHelper = new TicketCareDBHelper(this);
        editpostname = (EditText) findViewById(R.id.editPostName);
        editpostcategory = (EditText) findViewById(R.id.editPostCategory);
        editpostremain = (EditText) findViewById(R.id.editPostRemain);
        editstartdate = (EditText) findViewById(R.id.editStartDate);
        editenddate = (EditText) findViewById(R.id.editEndDate);
        editremark = (EditText) findViewById(R.id.editRemark);
        insertbtn = (Button) findViewById(R.id.isonshelfBtn);
        dbmsg = (TextView) findViewById(R.id.textView49);
        insertbtn.setOnClickListener(insert);
    }

    Button.OnClickListener insert = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            String name = editpostname.getText().toString();
            String category = editpostcategory.getText().toString();
            int remain = Integer.valueOf(editpostremain.getText().toString());
            String startdate = editstartdate.getText().toString();
            String enddate = editenddate.getText().toString();
            String remark = editremark.getText().toString();
            ContentValues values = new ContentValues();
            values.put(TicketCareContract.IsOnShelf.COLUMN_NAME_TICKET_NAME, name);
            values.put(TicketCareContract.IsOnShelf.COLUMN_NAME_TICKET_CATEGORY, category);
            values.put(TicketCareContract.IsOnShelf.COLUMN_NAME_REMAIN_QTY, remain);
            values.put(TicketCareContract.IsOnShelf.COLUMN_NAME_START_DATE, startdate);
            values.put(TicketCareContract.IsOnShelf.COLUMN_NAME_END_DATE, enddate);
            values.put(TicketCareContract.IsOnShelf.COLUMN_NAME_REMARK, remark);

            long rowID = ticketCareDBHelper.getWritableDatabase().insert(TicketCareContract.IsOnShelf.TABLE_NAME, null, values);

            Log.i(IsOnShelfInsert.class.getName(), "Insert record with Row ID: " + rowID );
            if (rowID != -1){
                dbmsg.setText("Save Successfully with RowID: " + rowID );
                // call the method to update your list view.
            } else {
                dbmsg.setText("Fail to save.");
                // Show the error message.
            }
        }
    };
}
