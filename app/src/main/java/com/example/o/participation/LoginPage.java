package com.example.o.participation;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ticketcare.database.TicketCareContract;
import com.ticketcare.database.TicketCareDBHelper;

public class LoginPage extends AppCompatActivity {

    TicketCareDBHelper ticketCareDBHelper;
    Button loginbtn,registerbtn;
    EditText loginpassword,loginphone;
    TextView showlogin;
    String id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);
        loginphone = (EditText)findViewById(R.id.editLoginPhone);
        loginpassword = (EditText)findViewById(R.id.editLoginPassword);


        registerbtn = (Button)findViewById(R.id.registeringBtn);
        loginbtn = (Button)findViewById(R.id.loginingBtn);
        registerbtn.setOnClickListener(registerbtnDO);
        loginbtn.setOnClickListener(loginbtnDO);

        Intent intent = this.getIntent();
        loginphone.setText(intent.getStringExtra("phone"));
        loginpassword.setText(intent.getStringExtra("password"));
        id =intent.getStringExtra("id");





    }

    Button.OnClickListener registerbtnDO = new Button.OnClickListener(){
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(LoginPage.this,RegisterPage.class);
            startActivity(intent);
        }
    };

    Button.OnClickListener loginbtnDO = new Button.OnClickListener(){
        @Override
        public void onClick(View v) {
            ticketCareDBHelper = new TicketCareDBHelper(LoginPage.this);
            Cursor cursor = ticketCareDBHelper.getReadableDatabase().query(TicketCareContract.Volunteer.TABLE_NAME, null, null, null, null, null, null);
            String phone = loginphone.getText().toString();
            String password = loginpassword.getText().toString();



            int rows_num = cursor.getCount();
            if (!"".equals(phone) && !"".equals(password)) {
                if (rows_num != 0) {
                    cursor.moveToFirst();            //將指標移至第一筆資料
                    for (int i = 0; i < rows_num; i++) {
                        String getphone = cursor.getString(2);
                        String getpassword = cursor.getString(3);

                        if (!phone.equals(getphone)||!password.equals(getpassword)) {
                            Toast.makeText(LoginPage.this,"密碼或是帳號輸入不正確",Toast.LENGTH_SHORT).show();
                        }else
                             {
                                Toast.makeText(LoginPage.this,"登入成功",Toast.LENGTH_SHORT).show();
                                SharedPreferences sh =getSharedPreferences("register",MODE_PRIVATE) ;
                                sh.edit().putString("Phone", phone).putString("Name",cursor.getString(1)).putString("volunteerid",id).commit();
                                Intent intent = new Intent(LoginPage.this,NewHome.class);

                                startActivity(intent);
                            }


                        cursor.moveToNext();        //將指標移至下一筆資料
                    }
                }
            }
        }
    };

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return false;
    }

}
