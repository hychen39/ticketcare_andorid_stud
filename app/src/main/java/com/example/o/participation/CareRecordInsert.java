package com.example.o.participation;

import android.content.ContentValues;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.ticketcare.database.TicketCareContract;
import com.ticketcare.database.TicketCareDBHelper;

public class CareRecordInsert extends AppCompatActivity {

    private TicketCareDBHelper ticketCareDBHelper;
    Button insertbtn;
    EditText editname;
    TextView dbmsg;
    String homeenvironment,diet,healthstatus;
    RadioButton tidy,messy,enough,lacking,healthy,ill;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_care_record_insert);
        ticketCareDBHelper = new TicketCareDBHelper(this);
        editname = (EditText)findViewById(R.id.editCareRecordName);
        tidy = (RadioButton)findViewById(R.id.radiobtn_tidy);
        messy = (RadioButton)findViewById(R.id.radiobtn_messy);
        enough = (RadioButton)findViewById(R.id.radiobtn_enough);
        lacking = (RadioButton)findViewById(R.id.radiobtn_lacking);
        healthy = (RadioButton)findViewById(R.id.radiobtn_healthy);
        ill = (RadioButton)findViewById(R.id.radiobtn_ill);
        insertbtn = (Button)findViewById(R.id.BtnCareRecordInsert);
        dbmsg = (TextView)findViewById(R.id.carerecordMsg);
        tidy.setOnCheckedChangeListener(choosehomeenvironment);
        messy.setOnCheckedChangeListener(choosehomeenvironment);
        enough.setOnCheckedChangeListener(choosediet);
        lacking.setOnCheckedChangeListener(choosediet);
        healthy.setOnCheckedChangeListener(choosehealthstatus);
        ill.setOnCheckedChangeListener(choosehealthstatus);
        insertbtn.setOnClickListener(insert);
    }
    CompoundButton.OnCheckedChangeListener choosehomeenvironment = new CompoundButton.OnCheckedChangeListener(){
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch(buttonView.getId()){
                case  R.id.radiobtn_tidy:
                    homeenvironment = tidy.getText().toString();
                    break;
                case  R.id.radiobtn_messy:
                    homeenvironment = tidy.getText().toString();
                    break;
            }
        }
    };

    CompoundButton.OnCheckedChangeListener choosediet = new CompoundButton.OnCheckedChangeListener(){
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch(buttonView.getId()){
                case  R.id.radiobtn_enough:
                    diet = enough.getText().toString();
                    break;
                case  R.id.radiobtn_messy:
                    diet = lacking.getText().toString();
                    break;
            }
        }
    };

    CompoundButton.OnCheckedChangeListener choosehealthstatus = new CompoundButton.OnCheckedChangeListener(){
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch(buttonView.getId()){
                case  R.id.radiobtn_healthy:
                    healthstatus = healthy.getText().toString();
                    break;
                case  R.id.radiobtn_ill:
                    healthstatus = ill.getText().toString();
                    break;
            }
        }
    };

    Button.OnClickListener insert = new Button.OnClickListener(){
        @Override
        public void onClick(View v) {
            String name = editname.getText().toString();
            ContentValues values = new ContentValues();
            values.put(TicketCareContract.CareRecord.COLUMN_NAME_CLIENT_NAME,name);
            values.put(TicketCareContract.CareRecord.COLUMN_NAME_HOME_ENVIRONMENT,homeenvironment);
            values.put(TicketCareContract.CareRecord.COLUMN_NAME_DIET,diet);
            values.put(TicketCareContract.CareRecord.COLUMN_NAME_HEALTH_STATUS,healthstatus);

            long rowID = ticketCareDBHelper.getWritableDatabase().insert(TicketCareContract.CareRecord.TABLE_NAME, null, values);

            Log.i(CareRecordInsert.class.getName(), "Insert record with Row ID: " + rowID );
            if (rowID != -1){
                dbmsg.setText("Save Successfully with RowID: " + rowID );
                // call the method to update your list view.
            } else {
                dbmsg.setText("Fail to save.");
                // Show the error message.
            }
        }

    };

}
