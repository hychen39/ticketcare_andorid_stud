package com.example.o.participation;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ticketcare.database.QuestionnaireAsyncTask;
import com.ticketcare.database.TicketCareDBClient;

public class Questionnaire extends Fragment {

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        NewHome newHome = (NewHome)activity;
    }

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return  inflater.inflate(R.layout.activity_questionnaire,container,false);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        String[] sids=new String[]{"0"};


        // 執行背景程式取得  web service 資料, 並更新 UI.
        QuestionnaireAsyncTask person = new QuestionnaireAsyncTask();
        // 告訴 DBClient 要在那一個 fragment 顯示資料
        person.setOutputUI(this);
        person.execute();




    }
}
