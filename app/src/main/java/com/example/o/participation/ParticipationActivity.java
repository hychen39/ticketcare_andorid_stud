package com.example.o.participation;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.ticketcare.database.TicketCareContract;
import com.ticketcare.database.TicketCareDBHelper;
import com.ticketcare.entities.Client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class ParticipationActivity extends AppCompatActivity  {

    ListView listView1;

TicketCareDBHelper ticketCareDBHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_participation);
       // prepareData();
        listView1 = (ListView)findViewById(R.id.listView);
        ticketCareDBHelper = new TicketCareDBHelper(this);
        Cursor cursor = ticketCareDBHelper.getReadableDatabase().query(TicketCareContract.ParticipationRecord.TABLE_NAME,null,null,null,null,null,null);
//        imageView =(ImageView)findViewById(R.id.imageView);
//        txname = (TextView)findViewById(R.id.ptname);
//        txphone= (TextView)findViewById(R.id.ptphone);
//        txaddress= (TextView)findViewById(R.id.ptaddress);

        SimpleCursorAdapter adapter  = new SimpleCursorAdapter(this
                ,android.R.layout.simple_expandable_list_item_2,cursor
                ,new String[]{"recordname","recordfrequency"}
                ,new int[]{android.R.id.text1,android.R.id.text2});

        listView1.setAdapter(adapter);
        listView1.setOnItemClickListener(partView);

    }


    /**
     *  To show the information for the selected client.
     */
    ListView.OnItemClickListener partView = new ListView.OnItemClickListener(){
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Cursor cursor = (Cursor)parent.getItemAtPosition(position);
            String name = cursor.getString(1);
            String phone = cursor.getString(2);
            String address = cursor.getString(3);
            String sex = cursor.getString(4);
            String habit = cursor.getString(5);
            String boringdate = cursor.getString(6);
            String idcard = cursor.getString(7);
            final Client client = new Client();



        }
    };

}
