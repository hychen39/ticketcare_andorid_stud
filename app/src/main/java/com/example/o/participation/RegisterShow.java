package com.example.o.participation;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.ticketcare.database.TicketCareDBClient;
import com.ticketcare.database.VolunteerAsyncTask;
import com.ticketcare.entities.VolunteerEntity;

import static android.content.Context.MODE_PRIVATE;
import static junit.framework.Assert.assertEquals;

public class RegisterShow extends Fragment {

    TextView volunteer_name,volunteer_phone;
    Button logout;
    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        NewHome newHome = (NewHome)activity;
    }

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return  inflater.inflate(R.layout.activity_register_show,container,false);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        volunteer_name = (TextView)getActivity().findViewById(R.id.get_register_nameTx);
        volunteer_phone= (TextView)getActivity().findViewById(R.id.get_register_phoneTx);



        String user_name = getActivity().getSharedPreferences("register", MODE_PRIVATE)
                .getString("Name", "");
        volunteer_name.setText(user_name);
        String user_phone = getActivity().getSharedPreferences("register", MODE_PRIVATE)
                .getString("Phone", "");
        volunteer_phone.setText(user_phone);

        logout = (Button)getActivity().findViewById(R.id.logout_btn);
        logout.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent =  new Intent(getActivity(),LoginPage.class);
                getActivity().startActivity(intent);
            }
        });




    }
}
