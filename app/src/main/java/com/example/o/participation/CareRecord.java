package com.example.o.participation;

import android.app.Activity;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ticketcare.database.TicketCareContract;
import com.ticketcare.database.TicketCareDBHelper;

public class CareRecord extends Fragment {

    private TicketCareDBHelper ticketCareDBHelper;
    ListView carerecordlist;

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        NewHome newHome = (NewHome)activity;
    }
    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return  inflater.inflate(R.layout.activity_care_record,container,false);

    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        ticketCareDBHelper = new TicketCareDBHelper(this.getActivity());
        carerecordlist = (ListView)getActivity().findViewById(R.id.care_record_list);
        Cursor cursor = ticketCareDBHelper.getReadableDatabase().query(TicketCareContract.CareRecord.TABLE_NAME,null,null,null,null,null,null);

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this.getActivity(),
                android.R.layout.simple_expandable_list_item_2,cursor,new String[]{"name","home_environment"}
                ,new int[] {android.R.id.text1, android.R.id.text2},0);
        carerecordlist.setAdapter(adapter);

    }
}
