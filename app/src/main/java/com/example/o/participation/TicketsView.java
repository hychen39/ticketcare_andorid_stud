package com.example.o.participation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ticketcare.entities.Ticket;

public class TicketsView extends AppCompatActivity {
    Ticket currentTicket;
    Button backtohome;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tickets_view);
        currentTicket = (Ticket) getIntent().getSerializableExtra("ticket");

        ImageView ticketImage = (ImageView) findViewById(R.id.imageTicket);
        ticketImage.setImageResource(currentTicket.getPicture());

        TextView ticketName = (TextView) findViewById(R.id.textticketsName);
        ticketName.setText(currentTicket.getTicketName());

        TextView ticketCategory = (TextView) findViewById(R.id.textticketsCategory);
        ticketCategory.setText(currentTicket.getCategory());

        TextView ticketUsingdate = (TextView) findViewById(R.id.textticketsusingDate);
        ticketUsingdate.setText(currentTicket.getUsingDate());

        TextView ticketDonor = (TextView) findViewById(R.id.textdonor);
        ticketDonor.setText(currentTicket.getDonor());

        TextView ticketAmount = (TextView) findViewById(R.id.textticketsAmount);
        ticketAmount.setText(String.valueOf(currentTicket.getAmount()));

        TextView ticketShowLocation = (TextView) findViewById(R.id.textshowLocation);
        ticketShowLocation.setText(currentTicket.getShowLocation());

        TextView ticketSynopsis = (TextView) findViewById(R.id.textsynopsis);
        ticketSynopsis.setText(currentTicket.getSynopsis());

        backtohome = (Button)findViewById(R.id.backtohome_ticket);
        backtohome.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TicketsView.this,NewHome.class);
                startActivity(intent);
            }
        });


    }
}
