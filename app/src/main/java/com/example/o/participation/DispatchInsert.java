package com.example.o.participation;

import android.content.ContentValues;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ticketcare.database.TicketCareContract;
import com.ticketcare.database.TicketCareDBHelper;

public class DispatchInsert extends AppCompatActivity {
    private TicketCareDBHelper ticketCareDBHelper;
    Button insertbtn;
    EditText editname,editcontent,editstatus;
    TextView dbmsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispatch_insert);
        ticketCareDBHelper =new TicketCareDBHelper(this);
        editname = (EditText)findViewById(R.id.editDispatchname);
        editcontent = (EditText)findViewById(R.id.editDispatchcontent);
        editstatus = (EditText)findViewById(R.id.editDispatchstatus);
        insertbtn = (Button)findViewById(R.id.dispatchinsertBtn);
        dbmsg  =(TextView)findViewById(R.id.textView23);
        insertbtn.setOnClickListener(insert);

    }
    Button.OnClickListener insert=new Button.OnClickListener(){
        @Override
        public void onClick(View v) {
            String name = editname.getText().toString();
            String content = editcontent.getText().toString();
            String status = editstatus.getText().toString();
            ContentValues values = new ContentValues();
            values.put(TicketCareContract.Dispatch.COLUMN_NAME_DISPATCHNAME,name);
            values.put(TicketCareContract.Dispatch.COLUMN_NAME_CONTENT,content);
            values.put(TicketCareContract.Dispatch.COLUMN_NAME_STATUS,status);


            long rowID = ticketCareDBHelper.getWritableDatabase().insert(TicketCareContract.Dispatch.TABLE_NAME, null, values);

            Log.i(DispatchInsert.class.getName(), "Insert record with Row ID: " + rowID );
            if (rowID != -1){
                dbmsg.setText("Save Successfully with RowID: " + rowID );
                // call the method to update your list view.
            } else {
                dbmsg.setText("Fail to save.");
                // Show the error message.
            }
        }

    };
}
