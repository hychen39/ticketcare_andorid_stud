package com.example.o.participation;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ticketcare.database.TakeTicketAsyncTask;
import com.ticketcare.entities.Client;
import com.ticketcare.entities.TakeTicketEntity;
import com.ticketcare.entities.Ticket;

public class TakingTicket extends AppCompatActivity {

    TextView ticketName,ticketCategory,ticketQty,ticketTakingDate,clientname,clientphone,clientaddress;
    Ticket ticket;
    Client client;
    Button donegiven;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taking_ticket);
        ticket = (Ticket)getIntent().getSerializableExtra("ticket");
        client = (Client)getIntent().getSerializableExtra("client");

        ticketName = (TextView)findViewById(R.id.taking_ticket_name);
        ticketName.setText(ticket.getTicketName());
        ticketCategory = (TextView)findViewById(R.id.taking_ticket_category);
        ticketCategory.setText(ticket.getCategory());
        ticketQty = (TextView)findViewById(R.id.taking_ticket_qty);
        ticketQty.setText(String.valueOf(ticket.getAmount()));
        ticketTakingDate = (TextView)findViewById(R.id.taking_date_txv);
        ticketTakingDate.setText(ticket.getUsingDate());

        clientname = (TextView)findViewById(R.id.client_name_tx);
        clientname.setText(client.getName());
        clientphone = (TextView)findViewById(R.id.client_phone_tx);
        clientphone.setText(client.getPhone());
        clientaddress = (TextView)findViewById(R.id.client_address_tx);
        clientaddress.setText(client.getAddress());

        donegiven = (Button)findViewById(R.id.donegiven_btn);
        donegiven.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(TakingTicket.this)
                        .setTitle("發放完畢")
                        .setMessage("是否已經將票券交自個案"  )
                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {


                                Intent intent = new Intent(TakingTicket.this, NewHome.class);

                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).show();
            }
        });


    }
}
