package com.example.o.participation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ticketcare.database.ReceiverFacade;
import com.ticketcare.database.TicketCareDBHelper;
import com.ticketcare.entities.Client;

public class PersonalInformationView extends AppCompatActivity {
    Client currentClient;
    Button backtohome;
    ImageView clientImage;
    DisplayMetrics mPhone;
    private TicketCareDBHelper ticketCareDBHelper;
    private static int CAM_REQUEST = 99;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_information_view);
        currentClient = (Client) getIntent().getSerializableExtra("client");
        // bind the data to view.


        TextView clientPhone = (TextView) findViewById(R.id.cellphoneTxv);
        clientPhone.setText(currentClient.getPhone().toString());

        TextView clientAddress = (TextView) findViewById(R.id.addressTxv);
        clientAddress.setText(currentClient.getAddress());

        TextView clientNote = (TextView)findViewById(R.id.noteTxv);
        clientNote.setText(currentClient.getNote());

        TextView clientSex = (TextView) findViewById(R.id.sexTxv);
        clientSex.setText(currentClient.getSex());

        TextView clientBoringDate = (TextView) findViewById(R.id.borningdateTxv);
        clientBoringDate.setText(currentClient.getBoringdate());

        TextView clientName = (TextView) findViewById(R.id.nameTxv);
        clientName.setText(currentClient.getName());

        // find the receiver's interests
        ReceiverFacade receiverFacade = new ReceiverFacade();

        // Return the interest in JSON format.
        String interest = receiverFacade.findInterest(currentClient.getId());
        currentClient.setHabit(interest);

        TextView clientHabit = (TextView)findViewById(R.id.habitTxv);
        clientHabit.setText(currentClient.getHabit());

        backtohome = (Button)findViewById(R.id.backtohome_btn);
        backtohome.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PersonalInformationView.this,NewHome.class);
                startActivity(intent);
            }
        });


//        //讀取手機解析度
//        mPhone = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(mPhone);
   }
//
//
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode,Intent data){
//        if(requestCode == CAM_REQUEST && data!=null){
//            Uri uri = data.getData();
//            ContentResolver cr = this.getContentResolver();
//
//            try{
////讀取照片，型態為Bitmap
//                Bitmap bitmap = BitmapFactory.decodeStream(cr.openInputStream(uri));
//
//                //判斷照片為橫向或者為直向，並進入ScalePic判斷圖片是否要進行縮放
//                if(bitmap.getWidth()>bitmap.getHeight())ScalePic(bitmap,
//                        mPhone.heightPixels);
//                else ScalePic(bitmap,mPhone.widthPixels);
//            }
//            catch (FileNotFoundException e)
//            {
//            }
//        }
//        super.onActivityResult(requestCode, resultCode, data);
//    }




//    private void ScalePic(Bitmap bitmap,int phone)
//    {
//        //縮放比例預設為1
//        float mScale = 1 ;
//
//        //如果圖片寬度大於手機寬度則進行縮放，否則直接將圖片放入ImageView內
//        if(bitmap.getWidth() > phone )
//        {
//            //判斷縮放比例
//            mScale = (float)phone/(float)bitmap.getWidth();
//
//            Matrix mMat = new Matrix() ;
//            mMat.setScale(mScale, mScale);
//
//            Bitmap mScaleBitmap = Bitmap.createBitmap(bitmap,
//                    0,
//                    0,
//                    bitmap.getWidth(),
//                    bitmap.getHeight(),
//                    mMat,
//                    false);
//            clientImage.setImageBitmap(mScaleBitmap);
//        }
//        else clientImage.setImageBitmap(bitmap);
//    }


}
