package com.example.o.participation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class Home extends AppCompatActivity {

    Button tickets,dispatch,cilentHistory,information,isonshelf,carerecord,
            clientinsert,ticketinsert,dispatchinsert,recordinsert,isonshelfinsert,carerecordinsert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //設定隱藏標題
        getSupportActionBar().hide();
        //設定隱藏狀態
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);

        tickets = (Button)findViewById(R.id.TicketBtn);
        dispatch = (Button)findViewById(R.id.DispatchBtn);
        cilentHistory = (Button)findViewById(R.id.CilentHistoryBtn);
        information = (Button)findViewById(R.id.PersonalInformationBtn);
        isonshelf = (Button)findViewById(R.id.IsonshelfBtn);
        carerecord = (Button)findViewById(R.id.CareRecordBtn);

        clientinsert=(Button)findViewById(R.id.ClientInstertBtn);
        ticketinsert=(Button)findViewById(R.id.TicketInsertBtn);
        dispatchinsert = (Button)findViewById(R.id.DispatchInsertBtn) ;
        recordinsert = (Button)findViewById(R.id.recordinsertBtn);
        isonshelfinsert= (Button)findViewById(R.id.IsonshelfinsertBtn);
        carerecordinsert=(Button)findViewById(R.id.CareRecordInsertBtn);

        tickets.setOnClickListener(ticketsDo);
        dispatch.setOnClickListener(dispatchDo);
        cilentHistory.setOnClickListener(cilentHistoryDO);
        information.setOnClickListener(informationDo);
        isonshelf.setOnClickListener(isonshelfDo);
        carerecord.setOnClickListener(carerecordDo);

        clientinsert.setOnClickListener(clientinsertDo);
        ticketinsert.setOnClickListener(ticketinsertDo);
        dispatchinsert.setOnClickListener(dispatchinsertDo);
        recordinsert.setOnClickListener(recordinsertDo);
        isonshelfinsert.setOnClickListener(isonshelfinsertDo);
        carerecordinsert.setOnClickListener(carerecordinsertDo);

    }

    Button.OnClickListener  ticketsDo = new Button.OnClickListener(){
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Home.this,Tickets.class);
            startActivity(intent);
        }
    };

    Button.OnClickListener dispatchDo = new Button.OnClickListener(){
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Home.this,Dispatch.class);
            startActivity(intent);
        }
    };

    Button.OnClickListener cilentHistoryDO = new Button.OnClickListener(){
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Home.this,ParticipationActivity.class);
            startActivity(intent);
        }
    };

    Button.OnClickListener informationDo = new Button.OnClickListener(){
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Home.this,PersonalInformation.class);
            startActivity(intent);
        }
    };

    Button.OnClickListener isonshelfDo = new Button.OnClickListener(){

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Home.this,IsOnShelf.class);
            startActivity(intent);
        }
    };

    Button.OnClickListener carerecordDo = new Button.OnClickListener(){
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Home.this,CareRecord.class);
            startActivity(intent);
        }
    };

    Button.OnClickListener clientinsertDo = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Home.this,ClientInsert.class);
            startActivity(intent);
        }
    };

    Button.OnClickListener ticketinsertDo = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(Home.this, TicketInsert.class);
            startActivity(intent);
        }
    };

    Button.OnClickListener dispatchinsertDo = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(Home.this,DispatchInsert.class);
            startActivity(intent);
        }
    };

    Button.OnClickListener recordinsertDo = new Button.OnClickListener(){
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(Home.this,RecordInsert.class);
            startActivity(intent);
        }
    };

    Button.OnClickListener isonshelfinsertDo = new Button.OnClickListener(){
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(Home.this,IsOnShelfInsert.class);
            startActivity(intent);
        }
    };

    Button.OnClickListener carerecordinsertDo =new Button.OnClickListener(){
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Home.this,CareRecordInsert.class);
            startActivity(intent);
        }
    };
}
