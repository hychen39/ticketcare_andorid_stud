package com.example.o.participation;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ticketcare.database.TicketCareContract;
import com.ticketcare.database.TicketCareDBHelper;
import com.ticketcare.database.VolunteerAsyncTask;
import com.ticketcare.entities.VolunteerEntity;

public class RegisterPage extends AppCompatActivity {

    private TicketCareDBHelper ticketCareDBHelper;
    EditText volunteername, volunteerphone, editpassword;
    Button registeredbtn;
    TextView dbMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_page);
        ticketCareDBHelper = new TicketCareDBHelper(this);
        volunteername = (EditText) findViewById(R.id.editVolunteerName);
        volunteerphone = (EditText) findViewById(R.id.editVolunteerPhone);
        editpassword = (EditText) findViewById(R.id.editPassword);

        registeredbtn = (Button) findViewById(R.id.registeredBtn);
        registeredbtn.setOnClickListener(registeredbtnDo);
    }

    Button.OnClickListener registeredbtnDo = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            String name = volunteername.getText().toString();
            String phone = volunteerphone.getText().toString();
            String password = editpassword.getText().toString();
            if ((name == null || "".equals(name)) || (phone == null || "".equals(phone)) || (password == null || "".equals(password)) || phone.length()<9) {
                Toast.makeText(RegisterPage.this, "資料都要填寫才能註冊喔！", Toast.LENGTH_SHORT).show();
            } else {

                VolunteerAsyncTask client = new VolunteerAsyncTask();

                ContentValues contentValues = new ContentValues();
                contentValues.put(TicketCareContract.Volunteer.COLUMN_NAME_VOLUNTEER_NAME, name);
                contentValues.put(TicketCareContract.Volunteer.COLUMN_NAME_PHONE, phone);
                contentValues.put(TicketCareContract.Volunteer.COLUMN_NAME_PASSWORD, password);

                Long rowID = ticketCareDBHelper.getWritableDatabase().insert(TicketCareContract.Volunteer.TABLE_NAME, null, contentValues);
                //Create a new ticket category entity
                VolunteerEntity volunteerEntity = new VolunteerEntity();
                volunteerEntity.setId(rowID);
                volunteerEntity.setName(name);
                volunteerEntity.setPassword(password);
                volunteerEntity.setPhone(phone);
                // call VolunteerAsyncTask to post in the background.
                client.execute(volunteerEntity);
                //String status = client.postVolunteerEntity(volunteerEntity);
                //assertEquals("204", status);


                Log.i(RegisterPage.class.getName(), "Insert record with Row ID: " + rowID);

                if (rowID != -1) {
                    Toast.makeText(RegisterPage.this,"註冊成功",Toast.LENGTH_SHORT).show();
                    // call the method to update your list view.
                    Intent intent = new Intent(RegisterPage.this, LoginPage.class);
                    intent.putExtra("id",rowID);
                    intent.putExtra("name", name);
                    intent.putExtra("phone", phone);
                    intent.putExtra("password", password);
                    startActivity(intent);
                } else {
//                    dbMsg.setText("Fail to save.");
                    // Show the error message.
                }
            }
        }
    };
}