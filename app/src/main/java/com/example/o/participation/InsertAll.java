package com.example.o.participation;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import static android.content.Context.MODE_PRIVATE;

public class InsertAll extends Fragment {
    Button person,ticket,dispatch,history,care;
    TextView setRegister;
    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        NewHome newHome = (NewHome)activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return  inflater.inflate(R.layout.insert_all,container,false);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        person = (Button)getActivity().findViewById(R.id.BtnPersonInsert);
        ticket = (Button)getActivity().findViewById(R.id.BtnTicketInsert);
        dispatch = (Button)getActivity().findViewById(R.id.BtnDispatchInsert);
        history = (Button)getActivity().findViewById(R.id.BtnHistoryInsert);
        care = (Button)getActivity().findViewById(R.id.BtnCareInsert);

        setRegister = (TextView)getActivity().findViewById(R.id.setRegisterText);
        String user = getActivity().getSharedPreferences("register", MODE_PRIVATE)
                .getString("Name", "");
        setRegister.setText(user);

        person.setOnClickListener(personinsertDo);
        ticket.setOnClickListener(ticketinsertDo);
        dispatch.setOnClickListener(dispatchinsertDo);
        history.setOnClickListener(historyinsertDo);
        care.setOnClickListener(careinsertDo);

    }

    Button.OnClickListener personinsertDo = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(InsertAll.this.getActivity(),ClientInsert.class);
            startActivity(intent);
        }
    };

    Button.OnClickListener ticketinsertDo = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(InsertAll.this.getActivity(), TicketInsert.class);
            startActivity(intent);
        }
    };

    Button.OnClickListener dispatchinsertDo = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(InsertAll.this.getActivity(),DispatchInsert.class);
            startActivity(intent);
        }
    };

    Button.OnClickListener historyinsertDo = new Button.OnClickListener(){
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(InsertAll.this.getActivity(),RecordInsert.class);
            startActivity(intent);
        }
    };


    Button.OnClickListener careinsertDo =new Button.OnClickListener(){
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(InsertAll.this.getActivity(),CareRecordInsert.class);
            startActivity(intent);
        }
    };
}
