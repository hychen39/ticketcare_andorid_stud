package com.example.o.participation;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.ticketcare.database.TicketCareContract;
import com.ticketcare.database.TicketCareDBHelper;
import com.ticketcare.entities.Client;
import com.ticketcare.entities.Ship;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class Dispatch extends Fragment {
  private TicketCareDBHelper ticketCareDBHelper;

    TextView timeview;
    private int mYear;
    private int mMonth;
    private int mDay;
    ListView dispatchList;

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        NewHome newHome = (NewHome)activity;
    }
    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return  inflater.inflate(R.layout.activity_dispatch,container,false);

    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        Calendar c=Calendar.getInstance();//建立抓日期物件c
        mYear=c.get(Calendar.YEAR);//年
        mMonth=c.get(Calendar.MONTH);//月
        mDay=c.get(Calendar.DAY_OF_MONTH);//日
        super.onCreate(savedInstanceState);

        timeview=(TextView)getActivity().findViewById(R.id.todayText);
        updateDisplay();
       // prepareData();
        dispatchList=(ListView)getActivity().findViewById(R.id.patch_list);

        ticketCareDBHelper = new TicketCareDBHelper(this.getActivity());
        Cursor cursor = ticketCareDBHelper.getReadableDatabase().query(TicketCareContract.Dispatch.TABLE_NAME,null,null,null,null,null,null);

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this.getActivity(),
                android.R.layout.simple_expandable_list_item_2,cursor,new String[]{"dispatchname","dispatchcontent"}
                ,new int[] {android.R.id.text1, android.R.id.text2},0);
        dispatchList.setAdapter(adapter);
        dispatchList.setOnItemClickListener(partview);


    }


    private void updateDisplay()
    {//設定tv的字，用append()方法加入字串，數字用自定方法轉成二個字元
        timeview.setText(
                new StringBuilder().append(mYear).append("年")
                        .append(format(mMonth + 1)).append("月")
                        .append(format(mDay)).append("日")
        );
    }

    //自定方法，如果只有一位數的時候就加入一個0
    private String format(int i) {
        String s=" "+i;
        if(s.length()==1) s="0"+s;
        return s;
    }

    ListView.OnItemClickListener partview = new ListView.OnItemClickListener(){

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Cursor cursor = (Cursor)parent.getItemAtPosition(position);
            String name = cursor.getString(1);
            String context = cursor.getString(2);
            String status = cursor.getString(3);
            final Ship selectShip = new Ship();
            selectShip.setName(name);
            selectShip.setContext(context);
            selectShip.setStatus(status);
            AlertDialog.Builder builder = new AlertDialog.Builder(Dispatch.this.getActivity());
            builder.setTitle("確認");
            builder.setMessage("是否要執行此派案");
            builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Dispatch.this.getActivity(),"???",Toast.LENGTH_SHORT).show();
                }
            });
            builder.setPositiveButton("接受", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Dispatch.this.getActivity(),DispatchView.class);
                    intent.putExtra("ship", selectShip);
                    startActivity(intent);
                }
            });
            builder.show();


        }
    };

}
