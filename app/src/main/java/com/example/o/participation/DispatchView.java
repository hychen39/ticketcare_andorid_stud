package com.example.o.participation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.ticketcare.entities.Client;
import com.ticketcare.entities.Ship;

public class DispatchView extends AppCompatActivity {

    Ship currentShip;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispatch_view);
        currentShip = (Ship) getIntent().getSerializableExtra("ship");
        // bind the data to view.

        TextView clientPhone = (TextView) findViewById(R.id.dpphone);
        clientPhone.setText(currentShip.getContext());

        TextView clientAddress = (TextView) findViewById(R.id.dpaddress);
        clientAddress.setText(currentShip.getStatus());

        TextView name = (TextView) findViewById(R.id.dpname);
        name.setText(currentShip.getName());
    }
}
