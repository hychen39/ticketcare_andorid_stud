package com.example.o.participation;

import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.ticketcare.database.TicketCareContract;
import com.ticketcare.database.TicketCareDBHelper;

public class MergeTickets extends AppCompatActivity {

    private TicketCareDBHelper ticketCareDBHelper;
    ListView listView;
    String currentHabit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merge_tickets);
        ticketCareDBHelper = new TicketCareDBHelper(this);
        currentHabit = getIntent().getStringExtra("habit");
        Cursor cursor = ticketCareDBHelper.getReadableDatabase().query(TicketCareContract.Tickets.TABLE_NAME,null,TicketCareContract.Tickets.COLUMN_NAME_TICKETCATEGORY+ "  = ?  ",new String[]{currentHabit},null,null,null);
        cursor.moveToFirst();
        Log.i("textttt",cursor.getColumnName(0));
        listView = (ListView)findViewById(R.id.mergingList);
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,android.R.layout.simple_expandable_list_item_2,cursor,new String[]{"ticketname","ticketcategory"},
                new int[]{android.R.id.text1,android.R.id.text2},0);
        listView.setAdapter(adapter);
    }
}
