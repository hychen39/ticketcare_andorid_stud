package com.example.o.participation;

import android.content.ContentValues;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ticketcare.database.TicketCareContract;
import com.ticketcare.database.TicketCareDBHelper;

public class ClientInsert extends AppCompatActivity {
    private TicketCareDBHelper ticketCareDBHelper;
    Button insertbtn;
    EditText edtname,edtphone,editaddress,editboringdate,editidcard,editsexual,edithabit;
    TextView dbMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_insert);

        ticketCareDBHelper = new TicketCareDBHelper(this);

        edtname = (EditText)findViewById(R.id.editName);
        edtphone = (EditText)findViewById(R.id.editPhone);
        editaddress = (EditText)findViewById(R.id.editAddress);
        editboringdate = (EditText)findViewById(R.id.editBoringdate);
        editidcard = (EditText)findViewById(R.id.editBoringdate);
        editsexual = (EditText)findViewById(R.id.editSexual);
        edithabit = (EditText)findViewById(R.id.editHabit);

        dbMsg = (TextView) findViewById((R.id.text_dbmsg));
        insertbtn = (Button)findViewById(R.id.insertBtn);
        insertbtn.setOnClickListener(insert);
    }

    Button.OnClickListener insert = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            String name = edtname.getText().toString();
            String phone = edtphone.getText().toString();
            String address = editaddress.getText().toString();
            String boringdate = editboringdate.getText().toString();
            String idcard = editidcard.getText().toString();
            String sexual = editsexual.getText().toString();
            String habit = edithabit.getText().toString();

            ContentValues values = new ContentValues();
            values.put(TicketCareContract.Clients.COLUMN_NAME_CLIENTNAME,name);
            values.put(TicketCareContract.Clients.COLUMN_NAME_PHONE, phone);
            values.put(TicketCareContract.Clients.COLUMN_NAME_ADDRESS, address);
            values.put(TicketCareContract.Clients.COLUMN_NAME_BORINGDATE,boringdate);
            values.put(TicketCareContract.Clients.COLUMN_NAME_IDCARD, idcard);
            values.put(TicketCareContract.Clients.COLUMN_NAME_SEXUAL, sexual);
            values.put(TicketCareContract.Clients.COLUMN_NAME_HABIT, habit);
            // Add value for Address field.

            long rowID = ticketCareDBHelper.getWritableDatabase().insert(TicketCareContract.Clients.TABLE_NAME, null, values);

            Log.i(ClientInsert.class.getName(), "Insert record with Row ID: " + rowID );
            if (rowID != -1){
                dbMsg.setText("Save Successfully with RowID: " + rowID );
                // call the method to update your list view.
            } else {
                dbMsg.setText("Fail to save.");
                // Show the error message.
            }
        }
    };



}
