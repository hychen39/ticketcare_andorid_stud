package com.example.o.participation;

import android.content.ContentValues;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ticketcare.database.TicketCareContract;
import com.ticketcare.database.TicketCareDBHelper;

public class TicketInsert extends AppCompatActivity {

    private TicketCareDBHelper ticketCareDBHelper;
    Button ticketinsertbtn;
    EditText editticketname,editticketcategory,editusingdate,editdonor,editshowlocation,editamount,editsysnopsis;
    TextView dbmsg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_insert);

        ticketCareDBHelper = new TicketCareDBHelper(this);

        editticketname = (EditText)findViewById(R.id.editTicketName);
        editticketcategory = (EditText)findViewById(R.id.editTicketCategory);
        editusingdate = (EditText)findViewById(R.id.editUsingDate);
        editdonor = (EditText)findViewById(R.id.editDonor);
        editshowlocation = (EditText)findViewById(R.id.editShowLocation);
        editamount = (EditText)findViewById(R.id.editAmount);
        editsysnopsis = (EditText)findViewById(R.id.editSysnopsis);

        dbmsg = (TextView)findViewById(R.id.textView36);

        ticketinsertbtn = (Button)findViewById(R.id.ticketInsertBtn);
        ticketinsertbtn.setOnClickListener(ticketInsert);
    }

    Button.OnClickListener ticketInsert = new Button.OnClickListener(){
        @Override
        public void onClick(View view) {
            String ticketname = editticketname.getText().toString();
            String ticketcategory = editticketcategory.getText().toString();
            String usingdate = editusingdate.getText().toString();
            String donor = editdonor.getText().toString();
            String showlocation = editshowlocation.getText().toString();
            int amount = 0;
            try {
                 amount = Integer.valueOf(editamount.getText().toString());
            } catch (NumberFormatException exception) {
                System.out.print("TicketInsert: the inputted amount is 0");
            }

            String sysnopsis = editsysnopsis.getText().toString();

            ContentValues values = new ContentValues();
            values.put(TicketCareContract.Tickets.COLUMN_NAME_TICKETNAME,ticketname);
            values.put(TicketCareContract.Tickets.COLUMN_NAME_TICKETCATEGORY,ticketcategory);
            values.put(TicketCareContract.Tickets.COLUMN_NAME_USINGDATE,usingdate);
            values.put(TicketCareContract.Tickets.COLUMN_NAME_DONOR,donor);
            values.put(TicketCareContract.Tickets.COLUMN_NAME_SHOWLOCATION,showlocation);
            values.put(TicketCareContract.Tickets.COLUMN_NAME_AMOUNT,amount);
            values.put(TicketCareContract.Tickets.COLUMN_NAME_SYSNOPSIS,sysnopsis);

            long rowID = ticketCareDBHelper.getWritableDatabase().insert(TicketCareContract.Tickets.TABLE_NAME, null, values);

            Log.i(ClientInsert.class.getName(), "Insert record with Row ID: " + rowID );
            if (rowID != -1){
                dbmsg.setText("Save Successfully with RowID: " + rowID );
                // call the method to update your list view.
            } else {
                dbmsg.setText("Fail to save.");
                // Show the error message.
            }

        }
    };
}
