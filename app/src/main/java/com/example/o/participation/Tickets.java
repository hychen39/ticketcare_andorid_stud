package com.example.o.participation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.ticketcare.database.TicketCareContract;
import com.ticketcare.database.TicketCareDBClient;
import com.ticketcare.database.TicketCareDBHelper;
import com.ticketcare.entities.Client;
import com.ticketcare.entities.Ticket;

import java.util.ArrayList;
import java.util.HashMap;

public class Tickets extends Fragment {
    private Context context;
    private TicketCareDBHelper  ticketCareDBHelper;

    ListView ticketsList ;

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        NewHome newHome = (NewHome)activity;
    }
    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return  inflater.inflate(R.layout.activity_tickets,container,false);

    }
@Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
         super.onActivityCreated(savedInstanceState);
        TicketCareDBClient ticketCareDBClient = new TicketCareDBClient();
        ticketCareDBClient.setOutputUI(this);
        ticketCareDBClient.execute();

//         Cursor cursor = ticketCareDBHelper.getReadableDatabase().query(TicketCareContract.Tickets.TABLE_NAME,null,null,null,null,null,null);
//         ticketsList = (ListView)getActivity().findViewById(R.id.listTickets);
//         SimpleCursorAdapter adapter = new SimpleCursorAdapter(this.getActivity(),
//                  android.R.layout.simple_expandable_list_item_2,cursor,new String[]{"ticketname","ticketcategory"}
//                  ,new int[] {android.R.id.text1, android.R.id.text2},0);
//         ticketsList.setAdapter(adapter);
//         ticketsList.setOnItemClickListener(partView);
    }

//    ListView.OnItemClickListener partView = new ListView.OnItemClickListener() {
//        @Override
//        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            Cursor cursor = (Cursor)parent.getItemAtPosition(position);
//            String name = cursor.getString(1);
//            String category = cursor.getString(2);
//            String usingDate = cursor.getString(3);
//            String donor = cursor.getString(4);
//            String showLocation = cursor.getString(5);
//            int amount = cursor.getInt(6);
//            String synopsis = cursor.getString(7);
//            Ticket selectTicket = new Ticket();
//            selectTicket.setTicketName(name);
//            selectTicket.setCategory(category);
//            selectTicket.setUsingDate(usingDate);
//            selectTicket.setDonor(donor);
//            selectTicket.setShowLocation(showLocation);
//            selectTicket.setAmount(amount);
//            selectTicket.setSynopsis(synopsis);
//            Intent intent = new Intent(Tickets.this.getActivity(),TicketsView.class);
//            intent.putExtra("ticket", selectTicket);
//            startActivity(intent);
//        }
//    };
}
