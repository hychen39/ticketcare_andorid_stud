package com.example.o.participation;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ticketcare.database.OnequestionnaireAsyncTask;
import com.ticketcare.database.QuestionnaireAsyncTask;
import com.ticketcare.database.TicketCareContract;
import com.ticketcare.database.TicketCareDBHelper;
import com.ticketcare.entities.Client;
import com.ticketcare.entities.QuestionnaireEntity;
import com.ticketcare.entities.Ticket;

import static junit.framework.Assert.assertEquals;

public class OneQuestionnaire extends AppCompatActivity {
    private TicketCareDBHelper ticketCareDBHelper;
    Ticket ticket;
    Client client;
    QuestionnaireEntity getquestionnaireEntity;
    TextView ticketname,ticketqty,clientname,clientphone;
    RadioGroup usedticket,beendone,gowith,upgrade,satisfy,continueing;
    String ticketuse,activityexperience,whouse,participations,satisfaction,nextchance,contents;
    Long ticketingid;
    EditText note;
    Button updateQuestionnaire;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_questionnaire);
        ticketCareDBHelper = new TicketCareDBHelper(this);

       ticket = (Ticket)getIntent().getSerializableExtra("ticket");
        client = (Client)getIntent().getSerializableExtra("client");
        getquestionnaireEntity = (QuestionnaireEntity) getIntent().getSerializableExtra("questionnaire");

        ticketname = (TextView)findViewById(R.id.questTicketName);
        ticketname.setText(ticket.getTicketName());
        ticketqty = (TextView)findViewById(R.id.questTicketQty);
        ticketqty.setText(String.valueOf(ticket.getAmount()));
        clientname = (TextView)findViewById(R.id.questPersonName);
        clientname.setText(client.getName());
        clientphone = (TextView)findViewById(R.id.questPersonPhone);
        clientphone.setText(client.getPhone());
        updateQuestionnaire = (Button)findViewById(R.id.upputQuestionnaire);
        updateQuestionnaire.setOnClickListener( new Button.OnClickListener(){

            @Override
            public void onClick(View v) {


               if(ticketuse==null||activityexperience==null||whouse==null||participations==null||satisfaction==null||nextchance==null||ticketingid==null){
                   Toast.makeText(OneQuestionnaire.this,"請確認是否全部填寫",Toast.LENGTH_SHORT).show();
               }else{

                   OnequestionnaireAsyncTask questionnaireAsyncTask = new OnequestionnaireAsyncTask();
                   ContentValues contentValues =new ContentValues();
                   contentValues.put(TicketCareContract.Questionnaire.COLUMN_NAME_TICKET_USE,ticketuse);
                   contentValues.put(TicketCareContract.Questionnaire.COLUMN_NAME_ACTIVITY_EXPERIENCE,activityexperience);
                   contentValues.put(TicketCareContract.Questionnaire.COLUMN_NAME_WHO_USE,whouse);
                   contentValues.put(TicketCareContract.Questionnaire.COLUMN_NAME_PARTICIPATION,participations);
                   contentValues.put(TicketCareContract.Questionnaire.COLUMN_NAME_SATISFACTION,satisfaction);
                   contentValues.put(TicketCareContract.Questionnaire.COLUMN_NAME_NEXT_CHANCE,nextchance);
                   contentValues.put(TicketCareContract.Questionnaire.COLUMN_NAME_CONTENT,contents);
                   contentValues.put(TicketCareContract.Questionnaire.COLUMN_NAME_TICKETING_ID,ticketingid);

                   long rowID = ticketCareDBHelper.getWritableDatabase().insert(TicketCareContract.Questionnaire.TABLE_NAME, null, contentValues);
                   Log.i("rowid:",String.valueOf(rowID));
                QuestionnaireEntity questionnaireEntity = new QuestionnaireEntity();
                questionnaireEntity.setId(rowID);
                questionnaireEntity.setTicketingId(ticketingid);
                questionnaireEntity.setIf_hadbeen(activityexperience);
                questionnaireEntity.setIfusing_ornot(ticketuse);
                questionnaireEntity.setIf_gowith(whouse);
                questionnaireEntity.setIf_upgrade(participations);
                questionnaireEntity.setIf_satisfaction(satisfaction);
                questionnaireEntity.setIf_continue(nextchance);
                questionnaireEntity.setEditNote(contents);
                questionnaireAsyncTask.execute(questionnaireEntity);
                //String status = questionnaireAsyncTask.postOnequestionnaireEntity(questionnaireEntity);
                //assertEquals("204", status);
                   new AlertDialog.Builder(OneQuestionnaire.this)
                           .setTitle("是否填寫完畢?")
                           .setMessage( "")
                           .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                               @Override
                               public void onClick(DialogInterface dialog, int which) {
                                   Intent intent = new Intent(OneQuestionnaire.this,NewHome.class);
                                   startActivity(intent);
                               }
                           })
                           .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                               @Override
                               public void onClick(DialogInterface dialog, int which) {

                               }
                           }).show();

               }
            }
        });

        ticketingid = getquestionnaireEntity.getId();


        usedticket = (RadioGroup)findViewById(R.id.ifusing_ornot);
         usedticket.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
         @Override
      public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.usingTicket_yes:
                        RadioButton yes = (RadioButton)findViewById(R.id.usingTicket_yes);
                        ticketuse = yes.toString();
                        break;
                    case R.id.usingTicket_no:
                        RadioButton no = (RadioButton)findViewById(R.id.usingTicket_no);
                        ticketuse = no.toString();
                        break;
                }
            }
        });
        beendone = (RadioGroup)findViewById(R.id.if_hadbeen);
        beendone.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.hadbeen_yes:
                        RadioButton yes = (RadioButton)findViewById(R.id.hadbeen_yes);
                        activityexperience = yes.toString();
                        break;
                    case R.id.hadbeen_no:
                        RadioButton no = (RadioButton)findViewById(R.id.hadbeen_no);
                        activityexperience = no.toString();
                        break;
                }
            }
        });
        gowith = (RadioGroup)findViewById(R.id.if_gowith);
        gowith.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.with_self:
                        RadioButton self = (RadioButton)findViewById(R.id.with_self);
                        whouse = self.toString();
                        break;
                    case R.id.with_family:
                        RadioButton family = (RadioButton)findViewById(R.id.with_family);
                        whouse = family.toString();
                        break;
                    case R.id.with_friend:
                        RadioButton friend = (RadioButton)findViewById(R.id.with_friend);
                        whouse = friend.toString();
                        break;
                    case R.id.with_register:
                        RadioButton register = (RadioButton)findViewById(R.id.with_register);
                        whouse = register.toString();
                        break;
                }
            }
        });
        upgrade = (RadioGroup)findViewById(R.id.if_upgrade);
        upgrade.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.upgrade_yes:
                        RadioButton yes = (RadioButton)findViewById(R.id.upgrade_yes);
                        participations = yes.toString();
                        break;
                    case R.id.upgrade_no:
                        RadioButton no = (RadioButton)findViewById(R.id.upgrade_no);
                        participations = no.toString();
                        break;
                }
            }
        });
        satisfy = (RadioGroup)findViewById(R.id.if_satisfaction);
        satisfy.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.satisfy_verygood:
                        RadioButton verygood = (RadioButton)findViewById(R.id.satisfy_verygood);
                        satisfaction = verygood.toString();
                        break;
                    case R.id.satisfy_good:
                        RadioButton good = (RadioButton)findViewById(R.id.satisfy_good);
                        satisfaction = good.toString();
                        break;
                    case R.id.satisfy_normal:
                        RadioButton normal = (RadioButton)findViewById(R.id.satisfy_normal);
                        satisfaction = normal.toString();
                        break;
                    case R.id.satisfy_soso:
                        RadioButton soso = (RadioButton)findViewById(R.id.satisfy_soso);
                        satisfaction = soso.toString();
                        break;
                    case R.id.satisfy_notgood:
                        RadioButton notgood = (RadioButton)findViewById(R.id.satisfy_notgood);
                        satisfaction = notgood.toString();
                        break;
                }
            }});
        continueing = (RadioGroup)findViewById(R.id.if_continue);
        continueing.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.keep_yes:
                        RadioButton keepyes = (RadioButton)findViewById(R.id.keep_yes);
                        nextchance = keepyes.toString();
                        break;
                    case R.id.keep_no:
                        RadioButton keepno = (RadioButton)findViewById(R.id.keep_no);
                        nextchance = keepno.toString();
                        break;
                }
       }
       });
        note = (EditText)findViewById(R.id.editNote);
        contents = note.getText().toString();




    }
}
