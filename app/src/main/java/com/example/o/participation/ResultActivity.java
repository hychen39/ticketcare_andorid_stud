package com.example.o.participation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.ticketcare.entities.Client;

public class ResultActivity extends AppCompatActivity {
    Client currentClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        currentClient = (Client) getIntent().getSerializableExtra("client");
        // bind the data to view.
//        ImageView clientImage = (ImageView) findViewById(R.id.ptvpicture);
//        clientImage.setImageResource(currentClient.getPicture());
        TextView clientPhone = (TextView) findViewById(R.id.ptvphone);
        clientPhone.setText(currentClient.getPhone());

        TextView clientAddress = (TextView) findViewById(R.id.ptvaddress);
        clientAddress.setText(currentClient.getAddress());

        TextView name = (TextView) findViewById(R.id.ptvname);
        name.setText(currentClient.getName());

    }
}
