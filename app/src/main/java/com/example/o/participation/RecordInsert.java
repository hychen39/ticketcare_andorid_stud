package com.example.o.participation;

import android.content.ContentValues;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ticketcare.database.TicketCareContract;
import com.ticketcare.database.TicketCareDBHelper;

import static android.R.attr.name;

public class RecordInsert extends AppCompatActivity {
    private TicketCareDBHelper ticketCareDBHelper;
    Button recordinsertbtn;
    EditText editrecordname,editrecordfrequency,editrecordreceived;
    TextView dbMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_insert);

        ticketCareDBHelper=new TicketCareDBHelper(this);

        editrecordname=(EditText)findViewById(R.id.editRecordName);
        editrecordfrequency=(EditText)findViewById(R.id.editFrequency);
        editrecordreceived=(EditText)findViewById(R.id.editReceived);
        dbMsg=(TextView) findViewById(R.id.text40);

        recordinsertbtn=(Button)findViewById(R.id.recordInsertBtn);
        recordinsertbtn.setOnClickListener(recordinsert);
    }

    Button.OnClickListener recordinsert=new Button.OnClickListener(){

        @Override
        public void onClick(View view) {
            String recordname=editrecordname.getText().toString();
            String recordfrenquency=editrecordfrequency.getText().toString();
            String recordreceived=editrecordreceived.getText().toString();

            ContentValues values=new ContentValues();
            values.put(TicketCareContract.ParticipationRecord.COLUMN_NAME_RECORDNAME,recordname);
            values.put(TicketCareContract.ParticipationRecord.COLUMN_NAME_FREQUENCY,recordfrenquency);
            values.put(TicketCareContract.ParticipationRecord.COLUMN_NAME_RECEIVED,recordreceived);



            long rowID=ticketCareDBHelper.getWritableDatabase().insert(TicketCareContract.ParticipationRecord.TABLE_NAME,null,values);
            if(rowID != -1){
                dbMsg.setText("Save Successfully with RowID:"+rowID);
            }else{
                dbMsg.setText("Fail to save");
            }
        }
    };





}
