package com.example.o.participation;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.ticketcare.database.TicketCareContract;
import com.ticketcare.database.TicketCareDBHelper;

/**
 *  檢視已上架的票卷
 */
public class IsOnShelf extends AppCompatActivity {
    private Context context;
    private TicketCareDBHelper ticketCareDBHelper;
    ListView isOnShelfList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isonshelf);

        ticketCareDBHelper = new TicketCareDBHelper(this);

        Cursor cursor = ticketCareDBHelper.getReadableDatabase().query(TicketCareContract.IsOnShelf.TABLE_NAME,null,null,null,null,null,null);
        isOnShelfList = (ListView)findViewById(R.id.ticket_list);

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_expandable_list_item_2,cursor,new String[]{"name","category"}
                ,new int[] {android.R.id.text1, android.R.id.text2},0);
        isOnShelfList.setAdapter(adapter);
    }
}
