package com.example.o.participation;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.ticketcare.database.ClientAsyncTask;
import com.ticketcare.database.TicketCareContract;
import com.ticketcare.database.TicketCareDBClient;
import com.ticketcare.database.TicketCareDBHelper;
import com.ticketcare.entities.Client;

import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class PersonalInformation extends Fragment {

    private TicketCareDBHelper ticketCareDBHelper;
    ListView listView1;
    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        NewHome newHome = (NewHome)activity;
    }
    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return  inflater.inflate(R.layout.activity_personal_information,container,false);

    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // 執行背景程式取得  web service 資料, 並更新 UI.
        ClientAsyncTask client = new ClientAsyncTask();
        // 告訴 DBClient 要在那一個 fragment 顯示資料
        client.setOutputUI(this);
        client.execute();
//        sqlite 資料庫查詢
//        ticketCareDBHelper = new TicketCareDBHelper(this.getActivity());
//              Cursor c = ticketCareDBHelper.getReadableDatabase().query(TicketCareContract.Clients.TABLE_NAME,null,null,null,null,null,null);
//
//        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this.getActivity(),
//                android.R.layout.simple_expandable_list_item_2,c,new String[]{"name","phone"}
//                ,new int[] {android.R.id.text1, android.R.id.text2},0);
//
//        listView1 = (ListView)getActivity().findViewById(R.id.listPersonalInformation);
//        listView1.setAdapter(adapter);
//        listView1.setOnItemClickListener(partView);

    }



//    ListView.OnItemClickListener partView = new ListView.OnItemClickListener(){
//        @Override
//        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//            Cursor cursor = (Cursor)parent.getItemAtPosition(position);
//            String name = cursor.getString(1);
//            String phone = cursor.getString(2);
//            String address = cursor.getString(3);
//            String boringdate = cursor.getString(4);
//            String idcard = cursor.getString(5);
//            String sex = cursor.getString(6);
//            String habit = cursor.getString(7);
//
////            Toast.makeText(view.getContext(),cursor.getString(5),Toast.LENGTH_SHORT).show();
//            final Client selectClient = new Client();
//            selectClient.setName(name);
//            selectClient.setPhone(phone);
//            selectClient.setAddress(address);
//            selectClient.setSex(sex);
//            selectClient.setHabit(habit);
//            selectClient.setBoringdate(boringdate);
//            selectClient.setIdcard(idcard);
//            Intent intent = new Intent(PersonalInformation.this.getActivity(),PersonalInformationView.class);
//            intent.putExtra("client", selectClient);
//            startActivity(intent);
//
//
//
//
//        }
//    };


}
