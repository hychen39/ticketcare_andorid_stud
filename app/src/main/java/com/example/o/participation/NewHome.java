package com.example.o.participation;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.ticketcare.database.TicketCareContract;

public class NewHome extends FragmentActivity {

    private FragmentTabHost fragmentTabHost = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_home);
         fragmentTabHost = (FragmentTabHost)findViewById(android.R.id.tabhost);

        fragmentTabHost.setup(this,getSupportFragmentManager(),R.id.container);

        fragmentTabHost.addTab(fragmentTabHost.newTabSpec("registershow")
                        .setIndicator("您的個人資料")
                        ,RegisterShow.class,null);

        fragmentTabHost.addTab(fragmentTabHost.newTabSpec("person")
                        .setIndicator("個案")
                        ,PersonalInformation.class,null);

        fragmentTabHost.addTab(fragmentTabHost.newTabSpec("ticket")
                        .setIndicator("票券")
                        ,Tickets.class,null);

        fragmentTabHost.addTab(fragmentTabHost.newTabSpec("taketicket")
                        .setIndicator("派案")
                ,TakeTicket.class,null);

        fragmentTabHost.addTab(fragmentTabHost.newTabSpec("questionnaire")
                        .setIndicator("關懷")
                        ,Questionnaire.class,null);


    }
}
